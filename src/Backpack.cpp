/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Backpack.cpp
 * @brief       :   Class for the Backpack, It's an portable inventory, who can be held by a character during is adventure
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>

#include "..\include\Backpack.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
void Backpack::Pack(shared_ptr<IObject> pObject)
{
    m_stack.push(pObject);
}

shared_ptr<IObject> Backpack::UnPack()
{
    shared_ptr<IObject> removedObject = nullptr;
    if (IsNotEmpty())
    {
        removedObject = m_stack.top();
        m_stack.pop();
    }
    else
    {
        cout << "Backpack is empty !!" << endl;
    }
    return removedObject;
}

bool Backpack::IsNotEmpty() const
{
    return !m_stack.empty();
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator