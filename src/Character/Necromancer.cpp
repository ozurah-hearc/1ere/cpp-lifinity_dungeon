/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Necromancer.cpp
 * @brief       :   Class for the Necromancer, a class of character (Wizard specialisation)
 *                  The necromancer is a wizard who deals with the deads. They can use their power and reanime the deads
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Character\Wizard.hpp"
#include "..\..\include\Character\Necromancer.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
Necromancer::Necromancer(int level, int strength, int agility, int intelligence, int perception, double hp, string name, int mana, shared_ptr<IObject> object) //Object has default
    : Wizard(level, strength, agility, intelligence, perception, hp, name, mana, object)
{
    //The backpack is empty (define in CharacterClass)
}

#pragma endregion Constructor

#pragma region Destructor
//Default
#pragma endregion Destructor

#pragma region Public Method
#pragma region Override
string Necromancer::GetClassTypeName() const
{
    return "Necromancer";
}
#pragma endregion Override

void Necromancer::RiseUndeads()
{
    if (m_mana >= COST_MANA_RISE_UNDEADS)
    {
        cout << "Come to me undeads, and kill him !" << endl;
        m_mana -= COST_MANA_RISE_UNDEADS;
    }
    else
    {
        cout << "My mana is too low to call you, my undeads friends :(" << endl;
    }
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator
