/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   1.0
 * @date        :   09.03.2021
 *
 * @file        :   CharacterClass.cpp
 * @brief       :   Class for the CharacterClass. A character class is the "type" (the class) of a character, like a warrior or a wizard.
 *                  This class contains the base attribute and commons methods of each type of character
 * @remark      :   This class isn't just named "class" because it's a reserved keyword of cpp
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>

#include "..\..\include\Character\CharacterClass.hpp"
#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Object\Sword.hpp"
#include "..\..\include\Object\Potion.hpp"
#include "..\..\include\Spell\ISpell.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"
#include "..\..\include\Log\Log.hpp"
#include "..\..\include\Log\LogCollection.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
int CharacterClass::GetAgility() const
{
    return m_agility;
}

int CharacterClass::GetStrength() const
{
    return m_strength;
}

int CharacterClass::GetIntelligence() const
{
    return m_intelligence;
}

int CharacterClass::GetHp() const
{
    return m_hp;
}

int CharacterClass::GetPerception() const
{
    return m_perception;
}

string CharacterClass::GetName() const
{
    return m_name;
}

shared_ptr<IObject> CharacterClass::GetObject() const
{
    return m_pObject;
}

shared_ptr<Backpack> CharacterClass::GetBackpack() const
{
    return m_backpack;
}

void CharacterClass::SetBackpack(shared_ptr<Backpack> backpack)
{
    m_backpack = backpack;
}

void CharacterClass::SetName(string name)
{
    m_name = name;
}

int CharacterClass::GetLevel() const
{
    return m_level;
}

shared_ptr<ISpell> CharacterClass::GetSpells()
{
    return m_spells;
}

#pragma endregion Getter / Setter

#pragma region Constructor

CharacterClass::CharacterClass() : CharacterClass(1, 0, 0, 0, 0, 0, "no_name")
{
}

CharacterClass::CharacterClass(const CharacterClass &character)
{
    m_level = character.m_level;
    m_strength = character.m_strength;
    m_agility = character.m_agility;
    m_intelligence = character.m_intelligence;
    m_hp = character.m_hp;
    m_perception = character.m_perception;
    m_statDice = character.m_statDice;

    m_name = character.m_name; //why not instr. below : https://stackoverflow.com/questions/12678819/how-to-copy-a-string-of-stdstring-type-in-c
    //m_name = new char[strlen(character.m_name) + 1];
    //strcpy(m_name, character.m_name);

    m_pObject = character.m_pObject;
    m_backpack = character.m_backpack;
    m_spells = character.m_spells;
}

CharacterClass::CharacterClass(int level, int strength, int agility, int intelligence, int perception, int hp, string name, shared_ptr<IObject> object)
    : m_level(level), m_strength(strength), m_agility(agility), m_intelligence(intelligence), m_perception(perception), m_hp(hp), m_name(name), m_pObject(object)
{
    //m_backpack = Backpack();
    SetBackpack(make_shared<Backpack>(Backpack()));

    //Log the creation of the character
    std::stringstream stream;
    stream << "Character created. "
           << "name = " << m_name << " ; Address = " << this;
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Character,
            stream.str()}));
}

#pragma endregion Constructor

#pragma region Destructor
CharacterClass::~CharacterClass()
{
    //Remplaced with smart pointer

    // delete m_pObject;
    // m_pObject = nullptr;

    // while (m_backpack->IsNotEmpty()) // Clear backpack of the character
    //     delete m_backpack->UnPack(); // Delete the object
    //delete m_backpack;
    //m_backpack = nullptr;
}
#pragma endregion Destructor

#pragma region Public Method

void CharacterClass::Show() const
{
    cout << "===============================" << endl;
    cout << "Character :\t" << m_name << endl;
    cout << "Class :\t" << GetClassTypeName() << endl;
    cout << "===============================" << endl;
    cout << "strength :\t" << m_strength << endl;
    cout << "agility :\t" << m_agility << endl;
    cout << "intelligence :\t" << m_intelligence << endl;
    cout << "HP :\t\t" << m_hp << endl;
    if (m_pObject != nullptr)
        cout << "Object :\t\t" << m_pObject->GetName() << endl;
}

void CharacterClass::Interact(const CharacterClass &character) const
{
    cout << "Hello valiant " << character.GetName() << "! I'm " << m_name << endl;
}

void CharacterClass::UseObject(CharacterClass *character)
{ //paramter can't be smart pointer, because the target use "this"
    auto target = character;
    if (target == nullptr)
        target = this;

    bool objectCorrectlyUsed = true;
    // std::cout << typeid(m_pObject).name() << "//" << typeid(*m_pObject).name() << "//" << typeid(Sword).name() << endl;
    string msg = "Unknow action with this object";
    if (typeid(*m_pObject) == typeid(Sword)) //typeid(*) --> content of what is pointed  (like Sword) // typeid() --> ptr on a object of class (IObject)
    {
        std::cout << "Deals damage with sword" << endl;
        target->SufferDamage(m_pObject->GetFeature());
    }
    else if (typeid(*m_pObject) == typeid(Potion)) //typeid(*) --> content of what is pointed  (like Sword) // typeid() --> ptr on a object of class (IObject)
    {
        std::cout << "Deals damage with potion" << endl;
        target->SufferDamage(m_pObject->GetFeature());
    }
    else
    {
        std::cout << "Unknow action with this object" << endl;
        objectCorrectlyUsed = false;
    }

    //Log the damage getting
    std::stringstream stream;
    auto logLevel = Log::LogLevel::Game;
    if (objectCorrectlyUsed)
    {
        stream << "Character " << this << " (" << GetName() << ")"
               << " use his object " << GetObject() << " (" << GetObject()->GetName() << ")"
               << " to " << character << " (" << character->GetName() << ").";
    }
    else
    {
        logLevel = Log::LogLevel::Critical;
        stream << "Character " << this << " (" << GetName() << ")"
               << " use his object " << GetObject() << " (" << GetObject()->GetName() << ")"
               << " but the object has no associated action  ";
    }
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            logLevel, Log::LogType::Fight,
            stream.str()}));
}

void CharacterClass::SufferDamage(int damage)
{
    int realDamage = damage;
    if (damage < 0)
        realDamage = 0;

    //Log the damage lose
    std::stringstream stream;
    stream << "Character " << this << " (" << GetName() << ")"
           << " suffered " << damage << " damage (Hp : " << GetHp() << " -> " << GetHp() - realDamage << ")";
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Fight,
            stream.str()}));

    m_hp -= realDamage;

    //Can't go under 0
    if (m_hp < 0)
        m_hp = 0;
}

void CharacterClass::Heal(int amount)
{
    std::stringstream stream;
    stream << "Character " << this << " (" << GetName() << ")"
           << " was healed by " << amount << " hp (Hp : " << GetHp() << " -> " << GetHp() + amount << ")";
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Fight,
            stream.str()}));

    m_hp += amount;
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
ostream &He_Arc::RPG::operator<<(ostream &s, const CharacterClass &character)
{
    // return s << character.Show() << endl;
    s << "===============================" << endl
      << "Character :\t" << character.m_name << endl
      << "Class :\t" << character.GetClassTypeName() << endl
      << "===============================" << endl
      << "strength :\t" << character.m_strength << endl
      << "agility :\t" << character.m_agility << endl
      << "intelligence :\t" << character.m_intelligence << endl
      << "HP :\t\t" << character.m_hp << endl;
    if (character.m_pObject != nullptr)
        s << "Object:\t\t" << character.m_pObject->GetName() << endl;

    return s;
}
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
CharacterClass &CharacterClass::operator=(const CharacterClass &character)
{
    if (this != &character)
    {
        m_level = character.m_level;
        m_strength = character.m_strength;
        m_agility = character.m_agility;
        m_intelligence = character.m_intelligence;
        m_hp = character.m_hp;
        m_perception = character.m_perception;
        m_statDice = character.m_statDice;

        m_name = character.m_name; //why not instr. below : https://stackoverflow.com/questions/12678819/how-to-copy-a-string-of-stdstring-type-in-c
        //m_name = new char[strlen(character.m_name) + 1];
        //strcpy(m_name, character.m_name);

        m_pObject = character.m_pObject;
        m_backpack = character.m_backpack;
        m_spells = character.m_spells;
    }
    return *this;
}
#pragma endregion Member Overload operator
