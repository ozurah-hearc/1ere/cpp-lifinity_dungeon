/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   17.05.2021
 *
 * @file        :   Skeleton.cpp
 * @brief       :   Class for the Skeleton, a class of monster (CharacterClass specialisation)
 *                  It's the dead characters of the lost times ?!
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Character\CharacterClass.hpp"
#include "..\..\include\Character\Skeleton.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
Skeleton::Skeleton(int level, int strength, int agility, int intelligence, int perception, double hp, string name, shared_ptr<Sword> sword) //Object has default
    : CharacterClass(level, strength, agility, intelligence, perception, hp, name, sword)
{
    // bones-rang = pun with "Bones" and "Boomrang"
    m_spells = make_shared<NormalDamageSpell>(NormalDamageSpell("Bones-Rang", RANGE_SKILL_BONESRANG, DAMAGE_SKILL_BONESRANG));
}

#pragma endregion Constructor

#pragma region Destructor
//Default
#pragma endregion Destructor

#pragma region Public Method
#pragma region Override
Skeleton *Skeleton::Clone() const
{
    return new Skeleton(*this);
}

void Skeleton::Show() const
{
    CharacterClass::Show();
}

void Skeleton::Interact(const CharacterClass &character) const
{
    std::cout << "*Realy, do you think you can interact with a skeleton ?*" << std::endl;
}
string Skeleton::GetClassTypeName() const
{
    return "Skeleton";
}
#pragma endregion Override

void Skeleton::UseSkill(CharacterClass *target)
{
    cout << GetName() << " prepare a tackle" << endl;
    auto isNormalSpell = dynamic_cast<NormalDamageSpell *>(m_spells.get());
    if (isNormalSpell != nullptr)
    {
        isNormalSpell->CastSpell(this, target);
    }
    else
    {
        cout << "Unhandle spell, sorry !" << endl;
        std::stringstream stream;
        stream << "The " << GetClassTypeName() << "(" << this << ") use an unhandled skill/spell";

        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Critical, Log::LogType::Skill,
                stream.str()}));
    }
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
Skeleton &Skeleton::operator=(const Skeleton &character)
{
    if (this != &character)
    {
        m_strength = character.m_strength;
        m_agility = character.m_agility;
        m_intelligence = character.m_intelligence;
        m_hp = character.m_hp;
        m_level = character.m_level;
        m_perception = character.m_perception;
        m_statDice = character.m_statDice;

        m_name = character.m_name; //why not instr. below : https://stackoverflow.com/questions/12678819/how-to-copy-a-string-of-stdstring-type-in-c
        //m_name = new char[strlen(character.m_name) + 1];
        //strcpy(m_name, character.m_name);

        m_pObject = character.m_pObject;
        m_backpack = character.m_backpack;
        m_spells = character.m_spells;
    }
    return *this;
}
#pragma endregion Member Overload operator
