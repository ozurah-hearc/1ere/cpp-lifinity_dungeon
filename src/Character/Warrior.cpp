/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Warrior.cpp
 * @brief       :   Class for the Warrior, a class of character (CharacterClass specialisation)
 *                  The warriors use sword (and shields) and their strength to accomplish anything
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Character\CharacterClass.hpp"
#include "..\..\include\Character\Warrior.hpp"
#include "..\..\include\Object\Sword.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
Warrior::Warrior(int level, int strength, int agility, int intelligence, int perception, double hp, string name, shared_ptr<IObject> object) //Object has default
    : CharacterClass(level, strength, agility, intelligence, perception, hp, name, object)
{
    m_spells = make_shared<NormalDamageSpell>(NormalDamageSpell("FULL POWER", RANGE_SKILL_FULLPOWER, DAMAGE_SKILL_FULLPOWER)); //"secret power" for warrior !
    // m_spells.push_back(make_shared<NormalDamageSpell>(NormalDamageSpell("FULL POWER", 0))); //For the fun (Warriors thinks they have a secret power, let see that with their damage !!)
    //The backpack is empty (define in CharacterClass)
}

#pragma endregion Constructor

#pragma region Destructor
//Default
#pragma endregion Destructor

#pragma region Public Method
#pragma region Override
Warrior *Warrior::Clone() const
{
    return new Warrior(*this);
}

string Warrior::GetClassTypeName() const
{
    return "Warrior";
}
void Warrior::Interact(const CharacterClass &character) const
{
    std::cout << "Be careful " << character.GetName() << ", I can kill you !" << std::endl;
}

void Warrior::UseSkill(CharacterClass *target)
{
    cout << "Concentrates (as best he can, but it's not great)" << endl;
    auto isNormalSpell = dynamic_cast<NormalDamageSpell *>(m_spells.get());
    if (isNormalSpell != nullptr)
    {
        isNormalSpell->CastSpell(this, target);
    }
    else
    {
        cout << "Unhandle spell, sorry !" << endl;
        std::stringstream stream;
        stream << "The " << GetClassTypeName() << "(" << this << ") use an unhandled skill/spell";

        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Critical, Log::LogType::Skill,
                stream.str()}));
    }
}
#pragma endregion Override

#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
Warrior &Warrior::operator=(const Warrior &character)
{
    if (this != &character)
    {
        m_strength = character.m_strength;
        m_agility = character.m_agility;
        m_intelligence = character.m_intelligence;
        m_hp = character.m_hp;
        m_level = character.m_level;
        m_perception = character.m_perception;
        m_statDice = character.m_statDice;

        m_name = character.m_name; //why not instr. below : https://stackoverflow.com/questions/12678819/how-to-copy-a-string-of-stdstring-type-in-c
        //m_name = new char[strlen(character.m_name) + 1];
        //strcpy(m_name, character.m_name);

        m_pObject = character.m_pObject;
        m_backpack = character.m_backpack;
        m_spells = character.m_spells;
    }
    return *this;
}
#pragma endregion Member Overload operator
