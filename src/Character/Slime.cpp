/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   17.05.2021
 *
 * @file        :   Slime.cpp
 * @brief       :   Class for the Slime, a class of monster (CharacterClass specialisation)
 *                  A slime is a (small) monster who can be compared to a puddle of water. You can seen them more often like as a drop of water.
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Character\CharacterClass.hpp"
#include "..\..\include\Character\Slime.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
Slime::Slime(int level, int strength, int agility, int intelligence, int perception, double hp, string name) //Object has default
    : CharacterClass(level, strength, agility, intelligence, perception, hp, name)
{
    m_spells = make_shared<NormalDamageSpell>(NormalDamageSpell("Takle", RANGE_SKILL_TACKLE, DAMAGE_SKILL_TACKLE));
}

#pragma endregion Constructor

#pragma region Destructor
//Default
#pragma endregion Destructor

#pragma region Public Method
#pragma region Override
Slime *Slime::Clone() const
{
    return new Slime(*this);
}

void Slime::Show() const
{
    CharacterClass::Show();
}

void Slime::Interact(const CharacterClass &character) const
{
    std::cout << "*the slime is staring at you*" << std::endl;
}
string Slime::GetClassTypeName() const
{
    return "Slime";
}
#pragma endregion Override

void Slime::UseSkill(CharacterClass *target)
{
    cout << GetName() << " prepare a tackle" << endl;
    auto isNormalSpell = dynamic_cast<NormalDamageSpell *>(m_spells.get());
    if (isNormalSpell != nullptr)
    {
        isNormalSpell->CastSpell(this, target);
    }
    else
    {
        cout << "Unhandle spell, sorry !" << endl;
        std::stringstream stream;
        stream << "The " << GetClassTypeName() << "(" << this << ") use an unhandled skill/spell";

        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Critical, Log::LogType::Skill,
                stream.str()}));
    }
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator
