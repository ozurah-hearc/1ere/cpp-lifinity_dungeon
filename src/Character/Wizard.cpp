/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Wizard.cpp
 * @brief       :   Class for the Wizard, a class of character (CharacterClass specialisation)
 *                  The wizards have the ability to use some spells. They solve the problems with their intelligence
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Spell\ISpell.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"
#include "..\..\include\Character\CharacterClass.hpp"
#include "..\..\include\Character\Wizard.hpp"
#include "..\..\include\Log\Log.hpp"
#include "..\..\include\Log\LogCollection.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
Wizard::Wizard(int level, int strength, int agility, int intelligence, int perception, double hp, string name, int mana, shared_ptr<IObject> object) //Object has default
    : CharacterClass(level, strength, agility, intelligence, perception, hp, name, object)
{
    m_mana = mana;
    m_spells = make_shared<NormalDamageSpell>(NormalDamageSpell("Fire Ball", RANGE_SKILL_FIREBALL, DAMAGE_SKILL_FIREBALL));
    // m_spells.push_back(make_shared<NormalDamageSpell>(NormalDamageSpell("Fire Ball", DAMAGE_SKILL_FIREBALL)));

    //The backpack is empty (define in CharacterClass)
}

#pragma endregion Constructor

#pragma region Destructor
//Default
#pragma endregion Destructor

#pragma region Public Method
#pragma region Override
Wizard *Wizard::Clone() const
{
    return new Wizard(*this);
}

void Wizard::Show() const
{
    CharacterClass::Show();
    cout << "Mana: " << m_mana << endl;
}

void Wizard::Interact(const CharacterClass &otherCharacter) const
{
    std::cout << "Hello " << otherCharacter.GetName() << ", I'm a powerful wizard !" << std::endl;
}

string Wizard::GetClassTypeName() const
{
    return "Wizard";
}

void Wizard::UseSkill(CharacterClass *target)
{

    if (m_mana >= COST_MANA_CAST_SPELL)
    {
        // For now, assuming class can only have 1 spell
        auto isNormalSpell = dynamic_cast<NormalDamageSpell *>(m_spells.get());
        if (isNormalSpell != nullptr)
        {
            isNormalSpell->CastSpell(this, target);

            m_mana -= COST_MANA_CAST_SPELL;
        }
        else
        {
            cout << "Unhandle spell, sorry !" << endl;
            std::stringstream stream;
            stream << "The " << GetClassTypeName() << "(" << this << ") use an unhandled skill/spell";

            LogCollection::GetInstance()->AddLog(
                make_unique<Log>(Log{
                    Log::LogLevel::Critical, Log::LogType::Skill,
                    stream.str()}));
        }
    }
    else
    {
        cout << "I haven't enough mana ..." << endl;
    }
}
#pragma endregion Override

void Wizard::RegenMana()
{
    auto amount = 0;

    //Get amount of the regen
    auto diceRes = m_statDice.Roll();
    //Roll success
    if (diceRes <= m_intelligence)
        amount = REGEN_MANA_AMOUT;
    //Roll Critical Success
    if (m_statDice.GetWasMaximalRoll())
        amount = REGEN_MANA_AMOUT * 2; //double regen !
    //Roll Critical fail
    if (m_statDice.GetWasMaximalRoll())
        amount = -REGEN_MANA_AMOUT; //Lose mana !
                                    //Roll Fail, no regen get

    //Log the action
    std::stringstream stream;
    stream << "Character " << this << " (" << GetName() << ")"
           << " regen his mana from " << m_mana << " to " << m_mana + amount;
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Character,
            stream.str()}));

    //Finalise the mana regen
    m_mana += amount;
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator
