/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   Utility.hpp
 * @brief       :   This class contains statics methods qualified as utility (not specific to 1 class)
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <tuple>
#include <chrono>
#include <ctime>

#include "..\include\Utility.hpp"
#include "..\include\Exception\TextOutOfDisplay.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Static Public Method
void Utility::ClearTerminal()
{
    // system ("CLS"); //Didn't work properly

    //Do a fake clear by add new line
    for (int i = 0; i < NB_LINE_CLEARING_SCREEN; ++i)
        cout << endl;
}

void Utility::WaitEnterForContinue(bool showMsg)
{
    if (showMsg)
        cout << "Hit enter to continue ..." << endl;

    //wait input validation (enter)
    //why sync + get instead https://stackoverflow.com/questions/9349575/cin-ignore-is-not-working
    std::cin.sync();
    std::cin.get();
    // cin.ignore();
}

string Utility::LeftPadding(int totalLenght, string text)
{
    //setw is for stream; use stream and convert it to string
    std::stringstream stream;
    stream << std::setw(totalLenght) << text;
    auto tmp = stream.str();
    return stream.str();
}

string Utility::LeftPadding(int totalLenght, char character)
{
    return LeftPadding(totalLenght, string(1, character));
}

string Utility::CenterTextWithSpace(int length, string text)
{
    //check if the text can be centered
    if (static_cast<int>(text.length()) > length)
        throw Ex_TextOutOfDisplay();

    int leftPad = (length + text.length()) / 2;
    return Utility::LeftPadding(leftPad, text);
}

string Utility::TabShift(int qty)
{
    string shift = "";
    for (int i = 0; i < qty; ++i)
        shift += "\t";
    return shift;
}

string Utility::GetTextLine(int length, string border, string text, bool center)
{
    if (static_cast<int>(text.length()) > length - 2) // -2 for left and right borders
        throw Ex_TextOutOfDisplay();

    string leftText = text;

    if (center)
        leftText = CenterTextWithSpace(length - 2, text); // -2 to write between the border

    auto tmp = LeftPadding(length - leftText.length() - 1, border);
    string result = border +
                    leftText +
                    LeftPadding(length - leftText.length() - 1, border); // -1 to not count the first border
    return result;
}

string Utility::GetBorder(int length, string border)
{
    string result = "";
    // a sequence like "+-" will be displayed as [length times "+."]
    // or if the border is like "+-", it will di
    for (int i = length; i > 0; --i)
        result += border;

    return result;
}

Utility::dtTuple Utility::GetDateTimeFromSysClock(const chrono::system_clock::time_point clock)
{
    //Source : https://www.tutorialspoint.com/cplusplus/cpp_date_time.htm
    // https://www.cplusplus.com/reference/ctime/tm/

    std::time_t time = chrono::system_clock::to_time_t(clock);
    struct tm *parts = std::localtime(&time);

    int year = 1900 + parts->tm_year;
    int month = 1 + parts->tm_mon;
    return make_tuple(year, month, parts->tm_mday, parts->tm_hour, parts->tm_min, parts->tm_sec);
}

string Utility::dtTupleToString(dtTuple dt)
{
    auto [year, month, day, hour, min, sec] = dt;
    std::stringstream stream;
    stream << year
           << "-" << std::setw(2) << std::setfill('0') << month
           << "-" << std::setw(2) << std::setfill('0') << day
           << "_" << std::setw(2) << std::setfill('0') << hour
           << "-" << std::setw(2) << std::setfill('0') << min
           << "-" << std::setw(2) << std::setfill('0') << sec;
    return stream.str();
}

#pragma endregion Static Public Method

#pragma region Public Method
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator