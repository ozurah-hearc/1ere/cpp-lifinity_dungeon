/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   17.06.2021
 *
 * @file        :   NormalDamageSpell.cpp
 * @brief       :   Class for the normal damage spell/skill, that means it is rised by a character and  it only apply damage to a signle target
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <string>

#include "..\..\include\Spell\ISpell.hpp"
#include "..\..\include\Spell\NormalDamageSpell.hpp"
#include "..\..\include\Character\CharacterClass.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

string NormalDamageSpell::GetName() const
{
    return m_name;
}

int NormalDamageSpell::GetRange() const
{
    return m_range;
}

#pragma endregion Getter / Setter

#pragma region Constructor

NormalDamageSpell::NormalDamageSpell(string name, int range, int power) : m_power(power)
{
    m_range = range;
    m_name = name;
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
void NormalDamageSpell::CastSpell(CharacterClass *user, CharacterClass *target)
{
    if (target == nullptr)
        target = user;

    //Log the spell casting
    std::stringstream stream;
    stream << "Character " << user << " (" << user->GetName() << ")"
           << " deals damage with the spell " << GetName() << " to " << target << " (" << target->GetName() << ").";
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Fight,
            stream.str()}));

    //Do the action for using the spell
    if (target != user)
        cout << target->GetName() << ", get this " << m_name << " !!" << endl;
    else //auto damage
    {
        cout << "oops, I haven't enough focus ... *the skill deals damage to the user*" << endl;
        target = user;
    }

    target->SufferDamage(m_power);
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator