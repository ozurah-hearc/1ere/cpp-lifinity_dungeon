/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 *
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   4.0
 * @date        :   13.04.2021
 *
 * @file        :   Main.cpp
 * @brief       :   Main class of the program, which starts the game and calls other classes to perform the game
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <list>
#include <vector>
#include <string>

#include "..\include\Game.hpp"
#include "..\include\Character\CharacterClass.hpp"
#include "..\include\Character\Warrior.hpp"
#include "..\include\Character\Wizard.hpp"
#include "..\include\Character\Necromancer.hpp"
#include "..\include\Object\Sword.hpp"
#include "..\include\Object\Potion.hpp"
#include "..\include\Object\Shield.hpp"
#include "..\include\Dice.hpp"
#include "..\include\Room\Lane.hpp"
#include "..\include\Room\Room.hpp"
#include "..\include\Enums.hpp"
#include "..\include\Menu\Menu.hpp"
#include "..\include\Menu\MenuItem.hpp"
#include "..\include\Utility.hpp"
#include "..\include\Exception\TextOutOfDisplay.hpp"
#include "..\include\Exception\MenuItemOutOfMenuLimit.hpp"
#include "..\include\Exception\MenuItemNotFound.hpp"
#include "..\include\Exception\LogCantBeWrite.hpp"
#include "..\include\Exception\GamePlayerDie.hpp"
#include "..\include\Log\LogCollection.hpp"
#include "..\include\Log\Log.hpp"
#include "..\include\FileManager.hpp"

using namespace std;
using namespace He_Arc::RPG;

/**
 * @brief Number of character of the player when the game statrs
 * 
 */
const int NB_CHARACTER_PLAYER_TEAM = 3;
/**
 * @brief Healing factor for reward passing room
 * 
 */
const int HEAL_FACTOR_ROOM_PASSED = 1.5;
/**
 * @brief Run the "test program" to check if the behavior of the program is correct
 * 
 */
void PerformTest();
/**
 * @brief Run the game mode (like it would be the relase)
 * 
 */
void PerformGame();
/**
 * @brief Use this to prompt the "enter key" for passing to the next test (to simplify the reading of the log of the tests)
 * @remark the message is like "this test part end, press enter to pass to the next test"
 * 
 */
void EndTestPartWaitEnter();

int main()
{
    //PerformTest();
    PerformGame();
}

void PerformTest()
{
    //cout display option
    cout << std::boolalpha;
    //End cout display option

    cout << endl
         << endl
         << endl
         << "#### V1 TESTS #####" << endl;
    Warrior gimli(1, 10, 5, 1, 10, 20, "Gimli");
    Wizard gandalf(1, 2, 2, 10, 15, 10, "Gandalf");
    Necromancer sauron{1, 10, 10, 100, 30, 666, "Sauron", 100}; // meilleur initialisation (Init. uniformisée)

    //---- Testing the show method ----
    cout << gimli << endl;

    gandalf.Show();

    sauron.Show();
    cout << "" << endl;

    //---- Testing interact ----
    gandalf.Interact(gimli);
    gimli.Interact(sauron);
    sauron.Interact(gimli);

    //---- Testing wizard spell ----
    gandalf.UseSkill(&sauron);
    sauron.UseSkill(&gimli);
    sauron.RiseUndeads();

    //#### V4 ####
    cout << endl
         << endl
         << endl
         << "#### V4 TESTS #####" << endl;
    auto pCharacter1 = make_shared<Warrior>(Warrior{1, 10, 15, 5, 10, 30, "Ozurah"});
    auto pCharacter2 = make_shared<Wizard>(Wizard{1, 5, 10, 30, 20, 15, "Ayrice", 30});
    auto pCharacter3 = make_shared<Necromancer>(Necromancer{1, 10, 10, 15, 20, 20, "Yosine", 10});
    auto pCharacter4 = make_shared<Warrior>(Warrior{1, 20, 20, 1, 15, 20, "Autariah"});

    list<shared_ptr<CharacterClass>> team;
    team.push_back(pCharacter1);
    team.push_back(pCharacter2);
    team.push_back(pCharacter3);
    team.push_back(pCharacter4);

    for (const auto character : team)
        character->Show();

    //#### V5 ####
    cout << endl
         << endl
         << endl
         << "#### V5 TESTS #####" << endl;
    auto sword1 = make_shared<Sword>(Sword("Wood Sword", 10));
    auto sword2 = make_shared<Sword>(Sword("Beginner Sword", 7));
    auto sword3 = make_shared<Sword>(Sword("Iron Sword", 15));

    auto shield1 = make_shared<Shield>(Shield("Beginner Shield", 10));
    auto shield2 = make_shared<Shield>(Shield("Wood Shield", 12));

    auto potion1 = make_shared<Potion>(Potion("Poisonus Potion", 7));
    auto potion2 = make_shared<Potion>(Potion("Blind Potion", 9));

    list<shared_ptr<IObject>> objectForWarrior = {sword1, sword2, sword3, shield1, shield2, potion1, potion2};
    for (auto object : objectForWarrior)
    {
        pCharacter1->GetBackpack()->Pack(object);
    }

    //---- Show backpack content ----
    cout << pCharacter1->GetName() << " empties his bag to see it's contents. He had :" << endl;
    while (pCharacter1->GetBackpack()->IsNotEmpty())
    {
        auto lastRemove = pCharacter1->GetBackpack()->UnPack();
        cout << " - object : " << lastRemove->GetName() << " (feature : " << lastRemove->GetFeature() << ")" << endl;

        //The character forgot to retake is objects, they have unspawn ! ...
        // delete lastRemove; //... to free memory
        // lastRemove = nullptr;
    }

    EndTestPartWaitEnter();

    //#### Constom Version ####
    cout << endl
         << endl
         << endl
         << "#### CUSTOM VERSION TESTS #####" << endl;

    //---- FILE WRITE ----
    cout << "##-- TESTS for the writing system --##" << endl;
    string filename{"./bin/sample.txt"};
    //bool fileExist = FileManager::IsFileExists(filename);
    //bool fileOverwrite = FileManager::ConfirmBeforeOverwriteFile(filename);
    // bool writeResult = FileManager::WriteInFile(filename, "Some text for trying the file class in debug", true);
    // cout << "The file was written correctly ? : " << writeResult << endl;

    EndTestPartWaitEnter();
    //---- LOG ----
    cout << endl
         << endl
         << endl
         << "##-- TESTS for the Logging system --##" << endl;
    auto log1 = LogCollection::GetInstance();
    auto log2 = LogCollection::GetInstance();
    // LogCollection::GetInstance()->AddLog(make_unique<Log>(Log{Log::LogLevel::Debug, Log::LogType::Other, "A log message"}));
    log1->AddLog(make_unique<Log>(Log{Log::LogLevel::Debug, Log::LogType::Other, "A 1st log message"}));
    cout << "wait enter to let verify if log file is correctly updated";
    Utility::WaitEnterForContinue(false);
    log2->AddLog(make_unique<Log>(Log{Log::LogLevel::Debug, Log::LogType::Other, "A 2nd log message"}));
    cout << "wait enter to let verify if log file is correctly updated";
    Utility::WaitEnterForContinue(false);
    log1->SetAutoSave(false);
    log1->AddLog(make_unique<Log>(Log{Log::LogLevel::Debug, Log::LogType::Other, "A 3nd log message"}));
    cout << "wait enter to let verify if log file is correctly updated (must not have changed)";
    Utility::WaitEnterForContinue(false);
    log1->AddLog(make_unique<Log>(Log{Log::LogLevel::Debug, Log::LogType::Other, "A 4nd log message"}));
    log2->SaveLog();
    cout << "wait enter to let verify if log file is correctly updated";
    Utility::WaitEnterForContinue(false);
    //log2 must save log1 log cause they have the same address

    //Set autosave for viewing log of next test
    log1->SetAutoSave(true);

    EndTestPartWaitEnter();
    //---- Dice ----
    cout << endl
         << endl
         << endl
         << "##-- TESTS for the DICE system --##" << endl;
    auto myDice = Dice<int>({1, 2, 3, 4, 5, 6});
    auto myDice2 = Dice<int>({1, 2, 3, 4, 5, 6}); //The roll must differe of the dice 1
    auto myDice3 = Dice<int>({1, 2, 3});
    auto myDice4 = Dice<int>();  //Use the default value (regular dice)
    auto myDice5 = Dice<int>(4); //Use the int constructor (regular dice)
    auto myDice6 = Dice<string>({"Aie", "Ouch", "that's hurt !"});
    //auto myDice7 = Dice<string>(); //Not allowed (correct)

    myDice3.RemoveFace(2);
    for (int i = 0; i < 5; ++i)
    {
        cout << "new roll (" << i + 1 << ") : " << endl;
        cout << " - Dice 1 : " << myDice.Roll() << " / is min : " << myDice.GetWasMinimalRoll() << " / is max : " << myDice.GetWasMaximalRoll() << endl;
        cout << " - Dice 2 : " << myDice2.Roll() << " / is min : " << myDice2.GetWasMinimalRoll() << " / is max : " << myDice2.GetWasMaximalRoll() << endl;
        // The dice 3 must never show a 2 to the roll
        cout << " - Dice 3 : " << myDice3.Roll() << " / is min : " << myDice3.GetWasMinimalRoll() << " / is max : " << myDice3.GetWasMaximalRoll() << endl;
        cout << " - Dice 6 : " << myDice6.Roll() << " / is min : " << myDice6.GetWasMinimalRoll() << " / is max : " << myDice6.GetWasMaximalRoll() << endl;
    }

    EndTestPartWaitEnter();
    //---- Damage ----
    cout << endl
         << endl
         << endl
         << "##-- TESTS for the Damaging system --##" << endl;

    cout << "c1 hp = " << pCharacter1->GetHp() << " // c2 hp = " << pCharacter2->GetHp() << endl;
    pCharacter1->Interact(*pCharacter2);
    pCharacter1->UseObject(pCharacter2.get()); //&* convert smart pointer to pointer // This method requires pointer, because he use this
    cout << "c1 hp = " << pCharacter1->GetHp() << " // c2 hp = " << pCharacter2->GetHp() << endl;
    pCharacter2->UseSkill(pCharacter1.get());
    //Fail (autotarget)
    cout << pCharacter2->GetName() << " use a second spell ... but it failed !" << endl;
    pCharacter2->UseSkill();
    cout << "c1 hp = " << pCharacter1->GetHp() << " // c2 hp = " << pCharacter2->GetHp() << endl;

    EndTestPartWaitEnter();
    //---- Lane ----
    cout << endl
         << endl
         << endl
         << "##-- TESTS for the laning system --##" << endl;
    Lane lane; //dont write lane(), else it would be considered like a method and not an object

    lane.ShowLaneInfo();
    lane.AddCharacter(pCharacter1, TeamGrp::Team1);
    lane.AddCharacter(pCharacter2, TeamGrp::Team1);
    lane.AddCharacter(pCharacter3, TeamGrp::Team2); //Won't be add, because not same team
    lane.ShowLaneInfo();

    lane.RemoveCharacter(pCharacter1);
    lane.ShowLaneInfo();
    lane.RemoveCharacter(pCharacter2);
    //Removed last character --> lane owner should be set to none team
    lane.ShowLaneInfo();

    auto testLaneAvailability1 = lane.IsLaneAvailableFor(TeamGrp::Team1);
    lane.AddCharacter(pCharacter1, TeamGrp::Team1);
    auto testLaneAvailability2 = lane.IsLaneAvailableFor(TeamGrp::Team2);
    auto testLaneAvailability3 = lane.IsLaneAvailableFor(TeamGrp::Team1);
    cout << "Test lane availability : " << endl;
    cout << " - for team 1 : value = " << testLaneAvailability1 << " / excepted = " << true << endl;
    cout << " - for team 2, already posseded by team 1 : value = " << testLaneAvailability2 << " / excepted = " << false << endl;
    cout << " - for team 1, already posseded by team 1 : value = " << testLaneAvailability3 << " / excepted = " << true << endl;
    lane.AddCharacter(pCharacter2, TeamGrp::Team1);
    lane.AddCharacter(pCharacter3, TeamGrp::Team1);
    // Testing the graphical lane
    auto dispLane = lane.GetSequenceDisplayLane();
    while (!dispLane.empty())
    {
        cout << dispLane.front() << endl;
        dispLane.pop();
    }
    dispLane = lane.GetSequenceDisplayLane(6);
    while (!dispLane.empty())
    {
        cout << dispLane.front() << endl;
        dispLane.pop();
    }

    EndTestPartWaitEnter();
    //---- Room ----
    cout << endl
         << endl
         << endl
         << "##-- TESTS for the room (multi laning) system --##" << endl;
    Room room{vector<shared_ptr<CharacterClass>>{pCharacter1, pCharacter2}, vector<shared_ptr<CharacterClass>>{pCharacter3, pCharacter4}};
    room.ShowRoomInfo();

    cout << "Team of the character 1 : " << TeamGrpToString[room.GetTeamOfCharacter(pCharacter1)] << endl;
    cout << "Team of the character 3 : " << TeamGrpToString[room.GetTeamOfCharacter(pCharacter3)] << endl;

    //Getting the lane of the character
    cout << "The character 1 is on this lane : " << endl;
    room.LaneNameToLane[room.GetLaneOfCharacter(pCharacter1)]->ShowLaneInfoCondensed(1);

    //Moving to a lane
    room.MoveCharacterToLane(pCharacter1, Room::LaneName::MidLaneLeftSide);
    cout << "The character 1 is now on this lane : " << endl;
    room.LaneNameToLane[Room::LaneName::MidLaneLeftSide]->ShowLaneInfoCondensed(1);

    //Moving 1 lane
    room.MoveCharacterOneLane(pCharacter1, false);
    cout << "The character 1 is no longer on this lane : " << endl;
    room.LaneNameToLane[Room::LaneName::MidLaneLeftSide]->ShowLaneInfoCondensed(1); //Character is no longer in this lane
    cout << "Now, the character 1 is on this lane : " << endl;
    room.LaneNameToLane[Room::LaneName::BackLaneLeftSide]->ShowLaneInfoCondensed(1); //Character is now here

    //Moving out of lane
    auto moveOutTest = room.MoveCharacterOneLane(pCharacter1, false);
    cout << "The character 1 haven't move, because he tried to go out of the fight lane ? " << !moveOutTest << endl;
    room.LaneNameToLane[Room::LaneName::BackLaneLeftSide]->ShowLaneInfoCondensed(1); //Character is now here

    //Unpossess a lane
    //The owner of the backlane pass from "Team1" to "None"
    room.MoveCharacterToLane(pCharacter2, Room::LaneName::DeadLeftSide); //To be sure the lane is empty
    room.MoveCharacterToLane(pCharacter1, Room::LaneName::DeadLeftSide);
    cout << "There is no people in the lane : " << !moveOutTest << endl;
    room.LaneNameToLane[Room::LaneName::BackLaneLeftSide]->ShowLaneInfoCondensed(1);

    //Unable to move in a lane possessed by other team
    room.MoveCharacterToLane(pCharacter1, Room::LaneName::BackLaneLeftSide);
    room.MoveCharacterToLane(pCharacter3, Room::LaneName::MidLaneLeftSide);

    //   The result should be "false" for each
    auto moveOnOtherTeamLaneTest = room.MoveCharacterOneLane(pCharacter1, true);
    cout << "The character 1 got the possibility to move on a team 2 lane possession ? " << moveOnOtherTeamLaneTest << endl;
    auto moveOnOtherTeamLaneTest2 = room.MoveCharacterToLane(pCharacter1, Room::LaneName::MidLaneLeftSide);
    cout << "\tAnd with the second method (like a teleportation) ? " << moveOnOtherTeamLaneTest2 << endl;

    // The room game display
    room.DisplayRoom();

    EndTestPartWaitEnter();
    //---- Menus ----
    Utility::ClearTerminal();

    cout << endl
         << endl
         << endl
         << "##-- TESTS for the menu system --##" << endl;

    try
    {
        Utility::CenterTextWithSpace(2, "word");
    }
    catch (Ex_TextOutOfDisplay const &e)
    {
        cout << "Trying to center a text without having the place to do it. Get the correct exception : Exception raised: " << e.what() << endl;
    }
    catch (...)
    {
        cout << "Trying to center a text without having the place to do it. Get the wrong exception" << endl;
    }

    Menu::DisplayHomePage();
    Menu::DisplayYouDiePage();

    auto pMenu1 = make_shared<MenuItem>(MenuItem("Action move (left)", MenuItem::MenuType::MoveLeft));
    auto pMenu2 = make_shared<MenuItem>(MenuItem("Action move (right)", MenuItem::MenuType::MoveRight));
    auto pMenu3 = make_shared<MenuItem>(MenuItem("Action fight", MenuItem::MenuType::Fight, pCharacter1));

    try
    {
        Menu menu0{"too much menu items", vector<shared_ptr<MenuItem>>{pMenu1, pMenu1, pMenu1, pMenu1, pMenu1,
                                                                       pMenu1, pMenu1, pMenu1, pMenu1, pMenu1,
                                                                       pMenu1, pMenu1, pMenu1, pMenu1, pMenu1,
                                                                       pMenu1, pMenu1, pMenu1, pMenu1, pMenu1,
                                                                       pMenu1, pMenu1, pMenu1, pMenu1, pMenu1, pMenu1}};
    }
    catch (Ex_MenuItemOutOfMenuLimit const &e)
    {
        cout << "Trying to display a menu with too much option. Get the correct exception : Exception raised: " << e.what() << endl;
    }
    catch (...)
    {
        cout << "Trying to display a menu with too much option. Get the wrong exception" << endl;
    }

    Menu menu1{"Round 1 : choose your action", vector<shared_ptr<MenuItem>>{pMenu1, pMenu2, pMenu3}};
    menu1.DisplayMenu();

    if (menu1.SelectMenu('B') == pMenu2)
        cout << "the second menu was selected programmatically" << endl;
    cout << menu1.SelectMenu('B').get()->GetMenuType() << endl;
    try
    {
        auto selectedMenu = menu1.AskMenuSelect();
        cout << "Test choosen menu = : " << selectedMenu;
        cout << endl
             << "the type of the menu is : " << menu1.SelectMenu(selectedMenu).get()->GetMenuType() << endl; //would be easier to see if text instead number
        cout << menu1.SelectMenu('Z') << endl;
    }
    catch (Ex_MenuItemNotFound const &e)
    {
        cout << "Exception raised: " << e.what() << endl;
    }

    //Test of an action on the linked element on the menu "3"
    auto menuSelected = menu1.SelectMenu('C');
    auto c = static_pointer_cast<Warrior>(menuSelected->GetLinkedElement()); //Now, we have the smart pointer of the warrior
    cout << "the element linked to the 3nd menu is the character " << c->GetName() << endl;

    //#### CLOSING / CLEARING Data ####
    // for (auto character : team)
    // {
    //     delete character;
    //     character = nullptr;
    // }

    EndTestPartWaitEnter(); //Only for putting a break point to observe elements before program ends
}

void PerformGame()
{
    //Set the logcollection info for a game
    LogCollection::GetInstance()->InitLogCol("./log/", "Game.log", true, true);

    //Empty terminal and display home page
    Utility::ClearTerminal();
    Menu::DisplayHomePage();

    Utility::ClearTerminal();

    Game game;
    game = Game(NB_CHARACTER_PLAYER_TEAM);
    // auto game = make_unique<Game>(Game(NB_CHARACTER_PLAYER_TEAM));

    try
    {
        //unique_ptr<Game> game = make_unique<Game>(Game(3)); //Maybe unique ptr would be better, but can't compile, not understand why ...

        //First step of the game, create the team
        game.CreateTeamComposition();

        Utility::WaitEnterForContinue(true);

        Utility::ClearTerminal();

        //Level 1
        cout << "You go into your first room." << endl
             << "Unfortunately, you need to kill the enemy before continuing your adventure" << endl
             << endl;

        game.PerformStartRoom();

        //Heal reward for passing the room
        game.HealPlayer(game.GetGameLevel()); //But not too much, this room was easy !

        // Level 2
        Utility::ClearTerminal();

        cout << "Well play ! you defeat the monster ! isn't too hard ?" << endl
             << "After this exploit, your team recovered a few health point, and deads recome alive !" << endl
             << "Now you access to the second room, this one is a little more difficult." << endl
             << "Good luck !" << endl
             << endl;

        game.PerformStartRoom();

        //Heal reward for passing the room
        game.HealPlayer(game.GetGameLevel());

        //Level 3+
        while (true) //Normaly not infinity loop like this, but it's for handling an exception somewhere ... and the game will
        {
            Utility::ClearTerminal();

            cout << "You win the fight... but ... how many time did you pass into this dungeon ? No one knows ..." << endl
                 << "The only thing that you are sure, is that you recovered a few HP for all of your companion, even the dead ! ... but will it be enough ?" << endl
                 << "Maybe you will nearly found the end ?" << endl
                 << " ... *or not ! GWA HA HA HA HA ... !!!* " << endl
                 << endl;
            game.PerformStartRoom();

            //Heal reword for passing the room
            game.HealPlayer(game.GetGameLevel() * HEAL_FACTOR_ROOM_PASSED); //The game comes harder, the heal is proportionnal to the level now
        }
    }
    catch (Ex_GamePlayerDie const &)
    {
        Utility::ClearTerminal();
        cout << "Finally you die ! It's not too late !!" << endl
             << "But Well play, you reach the room " << game.GetGameLevel() << endl
             << endl;
        Menu::DisplayYouDiePage();

        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Game, Log::LogType::Other,
                "Game ended after the player die, at room " + to_string(game.GetGameLevel())}));
    }
    catch (Ex_MenuItemOutOfMenuLimit const &)
    {
        cout << "Well play, you reach the limit of the menu; due to this, that means you have ended the \"demo\" of the game !" << endl;
        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Game, Log::LogType::Other,
                "Game ended after reach menu limit, end of the \"demo\" !"}));
    }
    catch (...)
    {
        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Critical, Log::LogType::Other,
                "An exception rise, and it wasn't handle !"}));
    }

    cout << endl; //Only for putting a break point to observe elements before program ends
}

void EndTestPartWaitEnter()
{
    cout << "## END of this TEST part ## ";
    Utility::WaitEnterForContinue(true);
}