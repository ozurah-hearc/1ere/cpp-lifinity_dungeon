/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   25.05.2021
 *
 * @file        :   Lane.cpp
 * @brief       :   A lane is a part of a room, one lane have 0-N characters on it.
 *                  It can be compared to the "cell" of the room
 *                  Only 1 team at once can go on a lane
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>
#include <queue>
#include <vector>
#include <algorithm> //Require for "remove"

#include "..\..\include\Room\Lane.hpp"
#include "..\..\include\Enums.hpp"
#include "..\..\include\Utility.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
vector<shared_ptr<CharacterClass>> Lane::GetCharactersOnLane() const
{
    return m_characters;
}

TeamGrp Lane::GetOwner() const
{
    return m_owner;
}

int Lane::GetNbCharacterOnTheRoom() const
{
    return m_characters.size();
}

#pragma endregion Getter / Setter

#pragma region Constructor
Lane::Lane(string name)
{
    m_name = name;
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
bool Lane::IsLaneEmpty() const
{
    return m_characters.empty();
}

bool Lane::IsCharacterOn(const shared_ptr<CharacterClass> &character) const
{

    return find(m_characters.begin(), m_characters.end(), character) != m_characters.end();
}

bool Lane::AddCharacter(shared_ptr<CharacterClass> character, TeamGrp team)
{
    bool resultAdd = false;

    if (IsLaneAvailableFor(team))
    {
        if (m_owner == TeamGrp::None) //Take owner of the lane
            m_owner = team;

        m_characters.push_back(character);

        resultAdd = true;
    }

    return resultAdd;
}

void Lane::RemoveCharacter(const shared_ptr<CharacterClass> &character)
{
    //https://www.javatpoint.com/cpp-algorithm-remove-function

    auto invalid = remove(m_characters.begin(), m_characters.end(), character); //remove elements
    m_characters.erase(invalid, m_characters.end());                            //Updating the sequence size

    if (IsLaneEmpty())
        m_owner = TeamGrp::None;
}

bool Lane::IsLaneAvailableFor(TeamGrp team) const
{
    return m_owner == TeamGrp::None || m_owner == team;
}

void Lane::ShowLaneInfo() const
{
    cout << "=============== LANE INFO ===============" << endl;
    cout << "Lane name : " << m_name << endl;
    cout << "This lane is owned by : " << TeamGrpToString[m_owner] << endl;
    if (m_characters.empty())
    {
        cout << "There is no character on this lane" << endl;
    }
    else
    {
        cout << "On the lane, there is :" << endl;

        for (auto character : m_characters)
        {
            cout << " - " << character->GetName() << endl;
        }
    }
    cout << "=============== END LANE INFO ===============" << endl;
}

void Lane::ShowLaneInfoCondensed(int nbShift) const
{
    string shift = Utility::TabShift(nbShift);

    cout << shift << "Lane name : " << m_name << endl;
    cout << shift << "\tOwned by : " << TeamGrpToString[m_owner] << endl;
    cout << shift << "\tCharacter on the lane : ";
    if (IsLaneEmpty())
        cout << "noone" << endl;
    else
    {
        for (auto character : GetCharactersOnLane())
        {
            cout << character->GetName() << " ";
        }
        cout << endl;
    }
}

queue<string> Lane::GetSequenceDisplayLane(int height) const
{

    queue<string> result;

    // Getting the size
    //Getting the longest name for the width
    int width = MIN_DISPLAY_WIDTH;
    for (auto character : m_characters)
    {
        if (width < static_cast<int>(character->GetName().size()) + DISPLAY_CHARACTERNAME_SPACING)
            width = character->GetName().size() + DISPLAY_CHARACTERNAME_SPACING;
    }

    // Getting the number of row
    if (height < GetNbCharacterOnTheRoom())
        height = GetNbCharacterOnTheRoom();
    if (height == 0)
        height = MIN_DISPLAY_HEIGHT; //At least 1 row

    //Set the header
    result.push((string) + "╒" + Utility::GetBorder(width, "═") + "╕");
    // syntax get from https://stackoverflow.com/questions/23936246/error-invalid-operands-of-types-const-char-35-and-const-char-2-to-binar
    // without, getting the error : error: invalid operands of types 'const char [2]' and 'const char [4]' to binary 'operator+'

    //Set the middle
    int centerNameDisplay = (height - GetNbCharacterOnTheRoom()) / 2;
    int charIndex = 0;
    for (int i = 0; i < height; ++i)
    {
        string charName = "";
        if (i >= centerNameDisplay && charIndex < GetNbCharacterOnTheRoom())
        {
            charName = m_characters[charIndex].get()->GetName();
            ++charIndex;
        }

        result.push(Utility::GetTextLine(width  +2, "|", charName, true)); //+2 because left and right border included

    }

    //Set the footer
    result.push((string) + "╘" + Utility::GetBorder(width, "═") + "╛");

    return result;
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator