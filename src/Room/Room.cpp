/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   25.05.2021
 *
 * @file        :   Room.cpp
 * @brief       :   The room is where the characters can do a fight.
 *                  It contains lanes, and handle the laning (move character on lanes)
 *                  The room also contains the team configuration infos
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>
#include <algorithm> //Require for "remove" and "insert"

#include "..\..\include\Room\Lane.hpp"
#include "..\..\include\Room\Room.hpp"
#include "..\..\include\Enums.hpp"
#include "..\..\include\Dice.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
Room::LaneName Room::GetLaneOfCharacter(const shared_ptr<CharacterClass> &character)
{
    Room::LaneName result = Room::LaneName::None;

    auto allLanes = GetAllLanes();
    int index = 0;
    //Check each lane to get the current lane of the character
    do
    {
        if (allLanes[index]->IsCharacterOn(character))
            result = LaneToLaneName[allLanes[index]];
        ++index;

    } while (result == Room::LaneName::None && index < static_cast<int>(allLanes.size()));

    return result;
}

vector<shared_ptr<Lane>> Room::GetAllLanes() const
{
    //merge all lanes
    auto allLanes(m_fightLanes);
    allLanes.insert(allLanes.end(), m_deadLanes.begin(), m_deadLanes.end());
    return allLanes;
}

vector<shared_ptr<CharacterClass>> Room::GetCharactersOnLane(const Room::LaneName laneName, bool onlyFightLane, TeamGrp specifiedTeam)
{
    vector<shared_ptr<CharacterClass>> result;

    if (specifiedTeam == TeamGrp::None || LaneNameToLane[laneName]->GetOwner() == specifiedTeam)
        result =  LaneNameToLane[laneName]->GetCharactersOnLane();

    return result;
}

vector<shared_ptr<CharacterClass>> Room::GetNeighbourCharacters(const shared_ptr<CharacterClass> source, int direction, TeamGrp specifiedTeam)
{
    auto baseLane = GetLaneOfCharacter(source);
    int neightbourLane = static_cast<int>(baseLane) + direction;

    vector<shared_ptr<CharacterClass>> result; //not null ptr, else return a vector of dim 1, with 1 ptr to 0x0
    if (IsFightLane(neightbourLane))
    {
        result = GetCharactersOnLane(static_cast<LaneName>(neightbourLane), true, specifiedTeam);
    }
    return result;
}
// Get the team vector of the character
// vector<CharacterClass *> *Room::GetTeamOfCharacter(CharacterClass &character) //if const here, can't assign the team result
// {
//     vector<CharacterClass *> *result = nullptr;
//     if (std::find(m_team1.begin(), m_team1.end(), character) != m_team1.end())
//     {
//         result = &m_team1;
//     }
//     else if (std::find(m_team2.begin(), m_team2.end(), character) != m_team2.end())
//     {
//         result = &m_team2;
//     }

//     return result;
// }

TeamGrp Room::GetTeamOfCharacter(const shared_ptr<CharacterClass> &character) //if const here, can't assign the team result
{
    auto result = TeamGrp::None;

    if (std::find(m_team1.begin(), m_team1.end(), character) != m_team1.end())
        result = TeamGrp::Team1;
    else if (std::find(m_team2.begin(), m_team2.end(), character) != m_team2.end())
        result = TeamGrp::Team2;

    return result;
}

#pragma endregion Getter / Setter

#pragma region Constructor
Room::Room(vector<shared_ptr<CharacterClass>> team1, vector<shared_ptr<CharacterClass>> team2)
    : m_team1(team1), m_team2(team2)
{
    m_fightLanes.push_back(m_backLaneLeftSide);
    m_fightLanes.push_back(m_midLaneLeftSide);
    m_fightLanes.push_back(m_frontLaneLeftSide);
    m_fightLanes.push_back(m_frontLaneRightSide);
    m_fightLanes.push_back(m_midLaneRightSide);
    m_fightLanes.push_back(m_backLaneRightSide);

    m_deadLanes.push_back(m_deadLaneLeftSide);
    m_deadLanes.push_back(m_deadLaneRightSide);

    RandomPlaceCharactersOnLanes();
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
bool Room::IsFightLane(const Room::LaneName lane) const
{
    return IsFightLane(static_cast<int>(lane));
}

bool Room::IsFightLane(const int lane) const
{
    return lane >= FIRST_FIGHT_LANE &&
           lane <= ROOM_FIGHT_LANE_SIZE;
}

bool Room::IsFightMoveAllowed(shared_ptr<CharacterClass> character, LaneName dest)
{
    return IsFightMoveAllowed(character, static_cast<int>(dest));
}

bool Room::IsFightMoveAllowed(shared_ptr<CharacterClass> character, int dest)
{ //is lane in the enum verification !
    return IsFightLane(dest) &&
           IsMoveAllowed(character, static_cast<LaneName>(dest));
}

bool Room::IsMoveAllowed(shared_ptr<CharacterClass> character, LaneName dest)
{
    return LaneNameToLane[dest]->IsLaneAvailableFor(GetTeamOfCharacter(character));
}

bool Room::MoveCharacterOneLane(shared_ptr<CharacterClass> character, bool isRightMove)
{
    bool result = false;

    //Determine the next lane
    int moveIndex = GetLaneOfCharacter(character);
    if (isRightMove)
        moveIndex++;
    else
        moveIndex--;

    if (IsFightMoveAllowed(character, moveIndex))
    {
        //Perform the lane move
        result = MoveCharacterToLane(character, static_cast<LaneName>(moveIndex));
    }

    return result;
}

bool Room::MoveCharacterToLane(shared_ptr<CharacterClass> character, LaneName destination)
{
    bool result = false;

    if (IsMoveAllowed(character, destination))
    {
        //Perform the lane move
        LaneNameToLane[GetLaneOfCharacter(character)]->RemoveCharacter(character);
        LaneNameToLane[destination]->AddCharacter(character, GetTeamOfCharacter(character));

        result = true;
    }

    return result;
}

void Room::MoveToDeadLane(shared_ptr<CharacterClass> character)
{
    if (GetTeamOfCharacter(character) == TeamGrp::Team1)
        MoveCharacterToLane(character, LaneName::DeadLeftSide);
    else
        MoveCharacterToLane(character, LaneName::DeadRightSide);
}

void Room::ShowRoomInfo() const
{
    cout << "=============== Room INFO ===============" << endl;
    cout << "Team 1 is composed by :" << endl;
    for (auto character : m_team1)
    {
        cout << " - " << character->GetName() << endl;
    }

    cout << "Team 2 is composed by :" << endl;
    for (auto character : m_team2)
    {
        cout << " - " << character->GetName() << endl;
    }

    cout << "For the lanes : " << endl;
    //merge all lanes
    auto allLanes = GetAllLanes();

    for (auto lane : allLanes)
    {
        lane->ShowLaneInfoCondensed(1);
    }
    cout << "=============== END ROOM INFO ===============" << endl;
}
void Room::DisplayRoom() const
{

    //Get the height for each lane
    int height = 0;
    for (auto lane : GetAllLanes())
    {
        if (lane->GetNbCharacterOnTheRoom() > height)
            height = lane->GetNbCharacterOnTheRoom();
    }

    //Get the graphic representation
    vector<queue<string>> displayLanes; //A "2D array" of display lane (1D = 1 lane; 2nd D = display lane)
    //Dead lane will be displayed differently
    for (auto lane : m_fightLanes)
    {
        displayLanes.push_back(lane->GetSequenceDisplayLane(height));
    }
    // Get the deadlane graphic (they will be used differently)
    auto lDeadLaneDisp = m_deadLaneLeftSide->GetSequenceDisplayLane(height);
    auto rDeadLaneDisp = m_deadLaneRightSide->GetSequenceDisplayLane(height);

    // Add 2 to height, because the sequenceDisplay have now a bottom and a top border
    height += 2;

    queue<string> allLaneDisplay;

    for (int heightIndex = 0; heightIndex < height; ++heightIndex)
    {
        string fullLaneRow = "";
        for (auto element : displayLanes)
        {
            //Removing the element at the start because (see comment below)
            for (int popIndex = 0; popIndex < heightIndex; ++popIndex)
                element.pop();

            fullLaneRow += element.front();
            //the pop element at the end will not remove the row in the displayLanes; it works on a copy !
            // element.pop();
        }
        allLaneDisplay.push(fullLaneRow);
    }

    //Now, display the result
    while (!allLaneDisplay.empty())
    {
        cout << lDeadLaneDisp.front() << " ✞ " << allLaneDisplay.front() << " ✞ " << rDeadLaneDisp.front() << endl;
        allLaneDisplay.pop();
        lDeadLaneDisp.pop();
        rDeadLaneDisp.pop();
    }
}
#pragma endregion Public Method

#pragma region Private Method
void Room::RandomPlaceCharactersOnLanes()
{
    /*
    //All on front lane (of their team)
    for (auto character : m_team1)
        m_FrontLaneLeftSide.AddCharacter(*character, TeamGrp::Team1);

    for (auto character : m_team2)
        m_FrontLaneRightSide.AddCharacter(*character, TeamGrp::Team2);
    */

    auto diceLanePlacement = Dice<int>(3);
    for (auto character : m_team1)
    {
        auto roll = diceLanePlacement.Roll();
        LaneNameToLane[static_cast<LaneName>(roll)]->AddCharacter(character, GetTeamOfCharacter(character));
    }

    for (auto character : m_team2)
    { //code repetition...
        auto roll = diceLanePlacement.Roll();
        LaneNameToLane[static_cast<LaneName>(roll + 3)]->AddCharacter(character, GetTeamOfCharacter(character));
        //+3 because enum "left side" is 4 to 6, and dice is 1,2,3
    }
}
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator

#pragma region Old code
/*
Room::LaneName Room::GetLaneName(const shared_ptr<Lane> lane) const
{
    LaneName result = LaneName::None;
    if (lane == m_backLaneLeftSide)
        result = LaneName::BackLaneLeftSide;
    else if (lane == m_midLaneLeftSide)
        result = LaneName::MidLaneLeftSide;
    else if (lane == m_frontLaneLeftSide)
        result = LaneName::FrontLaneLeftSide;
    else if (lane == m_frontLaneRightSide)
        result = LaneName::FrontLaneRightSide;
    else if (lane == m_midLaneRightSide)
        result = LaneName::MidLaneRightSide;
    else if (lane == m_backLaneRightSide)
        result = LaneName::BackLaneRightSide;
    else if (lane == m_deadLaneLeftSide)
        result = LaneName::DeadLeftSide;
    else if (lane == m_deadLaneRightSide)
        result = LaneName::DeadRightSide;

    return result;
}

shared_ptr<Lane> Room::GetLane(const LaneName lane)
{
    switch (lane)
    {
    case LaneName::BackLaneLeftSide:
        return m_backLaneLeftSide;
    case LaneName::MidLaneLeftSide:
        return m_midLaneLeftSide;
    case LaneName::FrontLaneLeftSide:
        return m_frontLaneLeftSide;
    case LaneName::FrontLaneRightSide:
        return m_frontLaneRightSide;
    case LaneName::MidLaneRightSide:
        return m_midLaneRightSide;
    case LaneName::BackLaneRightSide:
        return m_backLaneRightSide;
    case LaneName::DeadLeftSide:
        return m_deadLaneLeftSide;
    case LaneName::DeadRightSide:
        return m_deadLaneRightSide;

    default:
        return nullptr;
    }
}
*/
#pragma endregion Old code
