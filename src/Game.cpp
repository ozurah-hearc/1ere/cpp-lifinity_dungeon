/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   15.06.2021
 *
 * @file        :   Game.cpp
 * @brief       :   This class is the game logic implementation,
 *                  it regroups each classes and interconnect them to perform a complet game
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm> // std::random_shuffle + rotate

#include "..\include\Game.hpp"

#include "..\include\Character\CharacterClass.hpp"
#include "..\include\Character\Warrior.hpp"
#include "..\include\Character\Wizard.hpp"
#include "..\include\Character\Necromancer.hpp"
#include "..\include\Character\Slime.hpp"
#include "..\include\Character\Skeleton.hpp"
#include "..\include\Object\Sword.hpp"
#include "..\include\Object\Potion.hpp"
#include "..\include\Object\Shield.hpp"
#include "..\include\Dice.hpp"
#include "..\include\Room\Lane.hpp"
#include "..\include\Room\Room.hpp"
#include "..\include\Enums.hpp"
#include "..\include\Menu\Menu.hpp"
#include "..\include\Menu\MenuItem.hpp"
#include "..\include\Utility.hpp"
#include "..\include\Exception\GamePlayerDie.hpp"
#include "..\include\Exception\MenuItemNotFound.hpp"
#include "..\include\Log\LogCollection.hpp"
#include "..\include\Log\Log.hpp"
#include "..\include\FileManager.hpp"
#include "..\include\Spell\ISpell.hpp"
#include "..\include\Spell\NormalDamageSpell.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
int Game::GetGameLevel()
{
    return m_gameLevel;
}
#pragma endregion Getter / Setter

#pragma region Constructor
Game::Game(int nbAlly) : m_nbAlly(nbAlly)
{
}

Game::Game(const Game &game)
{
    m_nbAlly = game.m_nbAlly;
    m_playerTeam = game.m_playerTeam;
    m_ennemyTeam = game.m_ennemyTeam;
    m_fightOrder = game.m_fightOrder;

    m_gameLevel = game.m_gameLevel;

    //fightingRoom is unique ptr, but this room is created with the teams, just recreate a new room instead a "copy of unique_ptr" !
    if (m_fightingRoom != nullptr)
        m_fightingRoom = make_unique<Room>(Room(m_playerTeam, m_ennemyTeam));
    else
        m_fightingRoom = nullptr;
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
void Game::CreateTeamComposition()
{
    m_playerTeam.clear();

    //Team Creation
    while (static_cast<int>(m_playerTeam.size()) < m_nbAlly)
    {
        //Set the menu option
        //The characters are created here, and linked to the menu
        auto p2 = make_shared<Warrior>(Warrior());
        auto p1 = make_shared<Wizard>(Wizard());
        auto menuTeamSelecter = vector<shared_ptr<MenuItem>>{
            make_shared<MenuItem>(MenuItem("Character : Warrior", MenuItem::MenuType::CharacterSelection, make_shared<Warrior>(Warrior()))),
            make_shared<MenuItem>(MenuItem("Character : Wizard", MenuItem::MenuType::CharacterSelection, p1)),
            make_shared<MenuItem>(MenuItem("Character : Necromancer", MenuItem::MenuType::CharacterSelection, make_shared<Necromancer>(Necromancer()))),
        };

        // Set the menu
        Menu menu{"Team composition creation (member " + to_string((static_cast<int>(m_playerTeam.size()) + 1)) + ")", menuTeamSelecter};
        menu.DisplayMenu();

        //User input
        try
        {
            auto charSelected = menu.AskMenuSelect();
            auto selectedMenu = menu.SelectMenu(charSelected); //Rise exception if unknow menu

            //Get the character linked to the menu
            auto character = static_pointer_cast<CharacterClass>(selectedMenu->GetLinkedElement()); //Now, we have the smart pointer of the warrior

            //No need to create a character according the menu chosen, it is already created with default value
            //Just setting a name
            string charName{""};
            cout << "His name : ";
            cin >> charName;
            character->SetName(charName);

            //Add the character to the team
            m_playerTeam.push_back(character);

            //Save the info to the log
            LogCollection::GetInstance()->AddLog(make_unique<Log>(Log{Log::LogLevel::Game, Log::LogType::Character, "User create the character \"" + character->GetName() + "\", a " + character->GetClassTypeName()}));

            //Clear the terminal to have a "clean" output for the next action
            Utility::ClearTerminal();
        }
        catch (Ex_MenuItemNotFound const &e)
        {
            //Clean terminal before display the error, else the user will not see it
            Utility::ClearTerminal();

            cout << "Please, choose a valid class" << endl;
        }
    }

    cout << "So... your team members are :" << endl;
    for (auto character : m_playerTeam)
    {
        cout << " - " << character->GetName() << ", a " << character->GetClassTypeName() << endl;
    }
}

void Game::EnterFightingRoom()
{
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Room,
            "The player is now entering in a fight room ... preparation of the room"}));

    // Create ennemy team
    CreateEnnemyTeamComposition();

    //Set order
    SetTurnOrder();

    //Finalise the room
    m_fightingRoom = make_unique<Room>(Room(m_playerTeam, m_ennemyTeam));
}

void Game::DisplayFight() const
{
    // Player Team GUI
    cout << "Your team : " << endl;
    for (auto character : m_playerTeam)
    {
        cout << "   " << character->GetName() << " (" << character->GetClassTypeName() << "), HP: " << character->GetHp() << endl;
    }

    // Game Order GUI
    cout << endl
         << "Game order : " << endl
         << "   ";
    for (auto character : m_fightOrder)
    {
        cout << character->GetName() << " >> ";
    }
    //for having an indication "new turn" add again the "first character" to the last of the game turn
    cout << m_fightOrder[0]->GetName() << endl;

    // Room GUI
    cout << endl;
    m_fightingRoom->DisplayRoom();
}

void Game::PerformGameTurn()
{
    while (ThereIsDeadTeam() == TeamGrp::None)
    {
        auto team = m_fightingRoom->GetTeamOfCharacter(m_fightOrder[0]);
        if (team == TeamGrp::Team1)
            DoPlayerCharacterTurn();
        else if (team == TeamGrp::Team2)
            DoEnnemyCharacterTurn();

        auto noNeedGoNextPlayer = MoveDeadCharacters();

        if (!noNeedGoNextPlayer && ThereIsDeadTeam() == TeamGrp::None) //fight not finish
        {
            GetNextPlayer(); //Get the next character
        }
    }

    TeamGrp winner = TeamGrp::Team1;
    if (ThereIsDeadTeam() == TeamGrp::Team1)
        winner = TeamGrp::Team2;

    std::stringstream stream;
    stream << "The room fight is now ended ! "
           << " Looser are " << TeamGrpToString[ThereIsDeadTeam()]
           << ", winner are " << TeamGrpToString[winner];
    LogCollection::GetInstance()->AddLog(
        make_unique<Log>(Log{
            Log::LogLevel::Game, Log::LogType::Character,
            stream.str()}));
}

TeamGrp Game::ThereIsDeadTeam()
{
    TeamGrp result = TeamGrp::None;
    if (IsDeadTeam(m_playerTeam))
        result = TeamGrp::Team1;
    else if (IsDeadTeam(m_ennemyTeam))
        result = TeamGrp::Team2;

    return result;
}

bool Game::IsDeadTeam(vector<shared_ptr<CharacterClass>> team)
{
    bool aliveFound = false;
    int index = 0;
    while (!aliveFound && index < static_cast<int>(team.size()))
    {
        aliveFound = team[index]->GetHp() > 0;
        ++index;
    }

    return !aliveFound;
}

void Game::IncreaseGameLevel()
{
    ++m_gameLevel;
}

void Game::PerformStartRoom()
{
    IncreaseGameLevel();
    EnterFightingRoom();
    DisplayFight();
    Utility::WaitEnterForContinue(true);
    PerformGameTurn();

    if (ThereIsDeadTeam() == TeamGrp::Team1) //Just for having an handled exeception, throw it; normally this would not be like that !
        throw Ex_GamePlayerDie();
}

void Game::HealPlayer(int amount)
{
    for (auto character : m_playerTeam)
    {
        character->Heal(amount);
    }
}
#pragma endregion Public Method

#pragma region Private Method
void Game::CreateEnnemyTeamComposition()
{
    m_ennemyTeam.clear();

    // Set the possible ennemy, the order of the level is importent for the algorithm used in this method !
    // The level must be ordered from the lowest to the higher level
    list<shared_ptr<CharacterClass>> possibleEnnemy{
        make_shared<Slime>(Slime(1, 1, 1, 1, 2, 1, "Juvenile Slime")),
        make_shared<Skeleton>(Skeleton(1, 5, 3, 1, 2, 3, "injured Skeleton", make_shared<Sword>(Sword("Broken Sword", 2)))),
    };

    //Don't create usless ennemy if their are too high level
    if (m_gameLevel >= 2)
    {
        possibleEnnemy.push_back(make_shared<Slime>(Slime(2, 1, 2, 2, 2, 3, "Slime")));
    }
    if (m_gameLevel >= 3)
    {
        possibleEnnemy.push_back(make_shared<Warrior>(Warrior(3, 5, 3, 1, 2, 7, "Warrior", make_shared<Sword>(Sword("Little Sword", 1)))));
        possibleEnnemy.push_back(make_shared<Wizard>(Wizard(3, 1, 1, 5, 7, 5, "Wizard", 5)));
    }
    if (m_gameLevel >= 4)
    {
        possibleEnnemy.push_back(make_shared<Skeleton>(Skeleton(4, 5, 3, 1, 2, 13, "Skeleton", make_shared<Sword>(Sword("Old Sword", 6)))));
    }

    auto dice = Dice(possibleEnnemy);

    //The team will have the ennemies with the same total level as the game
    //Each time an ennemy is add to the list, his level will decrease this counter
    //And when the counter reach 0, that's mean the team is complet
    int lvlTeamSpace = m_gameLevel;

    while (lvlTeamSpace != 0)
    {
        //Get an entity, and add him to the ennemy team
        auto character = dice.Roll();
        //Because we will have the possibility of getting multiple times this character
        // we need to have a copy of the character, instead the pointer
        m_ennemyTeam.push_back(shared_ptr<CharacterClass>(character.get()->Clone()));
        //m_ennemyTeam.back().get()->SetName("Marco"); //Testing if the clone work
        lvlTeamSpace -= character->GetLevel();

        std::stringstream stream;
        stream << "Game add to the ennemy team the character \""
               << character->GetName()
               << "\", a " << character->GetClassTypeName()
               << " of level " << character->GetLevel()
               << " (address = " << character << ")";
        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Game, Log::LogType::Dice,
                stream.str()}));

        //Remove the too higher lvl ennemy from the dice
        //Because the "building list" for the dices is ordered from the lowest to the highest level,
        //We can iterate from the end to get the highers ennemy level
        //Until we have the same level as we can remove characters
        while (lvlTeamSpace > 0 && possibleEnnemy.back()->GetLevel() > lvlTeamSpace)
        {
            // //Remove the character of the dice
            dice.RemoveFace(possibleEnnemy.back());
            // //Remove the character of the possible list (for the next iteration)
            possibleEnnemy.pop_back();
        }
    }
}

void Game::SetTurnOrder()
{
    m_fightOrder = m_playerTeam;
    m_fightOrder.insert(m_fightOrder.end(), m_ennemyTeam.begin(), m_ennemyTeam.end());

    //random shuffle have a stange behaviore, it display an "error" in VSCode, but can compile
    // (and for both, random_shuffle and shuffle : the method is called (if breakpoint here) without programmatically call the method (when the program starts)
    //random_shuffle(m_fightOrder.begin(), m_fightOrder.end());
    shuffle(begin(m_fightOrder), end(m_fightOrder), default_random_engine{}); //Gen order will be always the same
}

void Game::GetNextPlayer()
{
    rotate(m_fightOrder.begin(), m_fightOrder.begin() + 1, m_fightOrder.end());
}

void Game::DoPlayerCharacterTurn()
{
    bool subMenuHasCancel;
    do
    {
        //This menu had a GUI, clean terminal before showing it's GUI
        Utility::ClearTerminal(); //Ready for the next GUI
        DisplayFight();

        //Set it to false, to cancel the previous action
        subMenuHasCancel = false;

        //Prepare the possible Action
        auto menu1Selecter = vector<shared_ptr<MenuItem>>{
            make_shared<MenuItem>(MenuItem("Pass", MenuItem::MenuType::Pass)),
            make_shared<MenuItem>(MenuItem("Move lane", MenuItem::MenuType::Move)),
            make_shared<MenuItem>(MenuItem("Hit", MenuItem::MenuType::Fight)),
        };

        Menu menu{"Turn of " + m_fightOrder[0]->GetName() + ". Choose his action", menu1Selecter};
        menu.DisplayMenu();

        shared_ptr<MenuItem> selectedMenu{nullptr};
        do
        {
            try
            {
                auto charSelected = menu.AskMenuSelect();
                selectedMenu = menu.SelectMenu(charSelected); //Rise exception if unknow menu
            }
            catch (Ex_MenuItemNotFound const &e)
            {
                cout << "Please, choose a valid action" << endl;
            }
        } while (selectedMenu == nullptr);

        // Perform the action of the selected menu
        switch (selectedMenu->GetMenuType())
        {
        case MenuItem::MenuType::Pass:
            DoActionMenuPass();
            break;
        case MenuItem::MenuType::Move:
            subMenuHasCancel = DoActionMenuMove();
            break;
        case MenuItem::MenuType::Fight:
        {
            subMenuHasCancel = DoActionMenuFight();
            break;
        }
        default: //Should not happend
            //Do nothing, like if "pass", without benefice for wizard
            break;
        }
    } while (subMenuHasCancel);
}

void Game::DoEnnemyCharacterTurn()
{
    //This menu had a GUI, clean terminal before showing it's GUI
    Utility::ClearTerminal(); //Ready for the next GUI
    DisplayFight();

    Menu currentActionDisplay{"Turn of " + m_fightOrder[0]->GetName() + ". He prepares his actions ", std::vector<shared_ptr<MenuItem>>()}; //no "choice" menu
    currentActionDisplay.DisplayMenu();

    auto maxRange = DoEnnemyTurnGetMaxRange();

    auto [isObjectCanBeUse, possibleSpells] = DoEnnemyTurnGetPossibleSpell(maxRange);

    if (!possibleSpells.empty())
    {
        auto [isObjectUse, spell] = DoEnnemyTurnGetRandomSpell(isObjectCanBeUse, possibleSpells);

        //Now, get targetable character with this spell
        int targetRange = 1; //For now, object have a range of 1
        if (!isObjectUse)
            targetRange = spell->GetRange();

        auto possibleTargets = DoEnnemyTurnGetPossibleTarget(targetRange);

        auto target = DoEnnemyTurnGetRandomTarget(possibleTargets);

        //Use spell
        DoEnnemyTurnUseSpell(isObjectUse, spell, target);
    }
    else // Can't hit ? move
    {
        DoEnnemyTurnMoveLeft();
    }

    //End of the ennemy turn, but let the possibility to the player to see the output message
    Utility::WaitEnterForContinue(true);
}

bool Game::MoveDeadCharacters()
{
    bool currentPlayerDie = false; //to avoid if the current player is die and pass the turn of the next character

    auto allCharacters = m_playerTeam;
    allCharacters.insert(allCharacters.end(), m_ennemyTeam.begin(), m_ennemyTeam.end());

    for (auto character : allCharacters)
    {
        if (character->GetHp() <= 0)
        {
            auto currentCharLane = m_fightingRoom->GetLaneOfCharacter(character);
            if (currentCharLane != Room::LaneName::DeadLeftSide && currentCharLane != Room::LaneName::DeadRightSide) //No need to do if already in dead lane
            {
                m_fightingRoom->MoveToDeadLane(character);

                if (character == m_fightOrder[0]) //Suicide
                    currentPlayerDie = true;

                //Remove the character from the "game order", cause he his dead !
                m_fightOrder.erase(remove(m_fightOrder.begin(), m_fightOrder.end(), character), m_fightOrder.end());

                //Log it
                std::stringstream stream;
                stream << "Character " << character << " (" << character->GetName() << ")"
                       << " is dead !";
                LogCollection::GetInstance()->AddLog(
                    make_unique<Log>(Log{
                        Log::LogLevel::Game, Log::LogType::Character,
                        stream.str()}));
            }
        }
    }

    return currentPlayerDie;
}

#pragma region Private Method(Enemy Action)
int Game::DoEnnemyTurnGetMaxRange()
{
    //Get the minimal range of each spell of the character
    int maxRange = 0;
    if (m_fightOrder[0]->GetObject() != nullptr)
        maxRange = 1; //for now, object have a range of 1

    //  for (auto spell : m_fightOrder[0]->GetSpells()) //Actually, only 1 spell
    auto spell = m_fightOrder[0]->GetSpells();
    {
        if (spell != nullptr)
            if (maxRange < spell->GetRange())
                maxRange = spell->GetRange();
    }

    return maxRange;
}

tuple<bool, vector<shared_ptr<ISpell>>> Game::DoEnnemyTurnGetPossibleSpell(int range)
{
    bool isObjectCanBeUse = m_fightOrder[0]->GetObject() != nullptr;

    //Get possible spell (if can hit someone)
    vector<shared_ptr<ISpell>> possibleSpells;
    for (int currentRange = -range; currentRange < 0; ++currentRange) //<0, if >=0 that means can't hit a player character; no need to check spells due to the game laning system
    {

        auto targets = m_fightingRoom->GetNeighbourCharacters(m_fightOrder[0], currentRange, TeamGrp::Team1);
        if (!targets.empty())
        {
            //Actually 1 spell, no need to check for each spells who can be used at this range
            possibleSpells.push_back(m_fightOrder[0]->GetSpells());
        }
        else if (currentRange == -1) //No target for the object (for now, object have a range of 1)
        {
            isObjectCanBeUse = false;
        }
    }
    //Clear the multiple entry in the possibleSpell (if spell can be used at range 1, 2; it will be availible 2 times in the possibleSpell)
    possibleSpells = Utility::distinctVector(possibleSpells);

    return {isObjectCanBeUse, possibleSpells};
}

tuple<bool, shared_ptr<ISpell>> Game::DoEnnemyTurnGetRandomSpell(bool includeObject, vector<shared_ptr<ISpell>> possibleSpells)
{
    // Add a "false" spell, for linking the object; it's simplify the dice roll
    auto tmpRefObjectSpell = make_shared<NormalDamageSpell>(NormalDamageSpell("object", 1, 0));

    if (includeObject)
        possibleSpells.push_back(tmpRefObjectSpell);

    // Dice for getting Get a random spell
    auto diceSpell = Dice(possibleSpells);

    //Get  spell
    auto spell = diceSpell.Roll();

    return {spell == tmpRefObjectSpell, spell};
}

vector<shared_ptr<CharacterClass>> Game::DoEnnemyTurnGetPossibleTarget(int range)
{
    //Dice for getting a random target
    //Get all target who can be targeted by a spell

    vector<shared_ptr<CharacterClass>> possibleTargets;
    for (int currentRange = -range; currentRange < 0; ++currentRange)
    { //<0, if >=0 that means can't hit a player character; no need to check spells due to the game laning system
        //Get the possible target for actual range
        auto targetsAtThisRange = m_fightingRoom->GetNeighbourCharacters(m_fightOrder[0], currentRange, TeamGrp::Team1);

        //Merge the target for this range with current possible target
        possibleTargets.insert(possibleTargets.end(), targetsAtThisRange.begin(), targetsAtThisRange.end());

        //no need to disctinct the vector; by the game logic, a character isn't on multiple lane
    }
    return possibleTargets;
}

shared_ptr<CharacterClass> Game::DoEnnemyTurnGetRandomTarget(vector<shared_ptr<CharacterClass>> possibleTargets)
{
    auto diceTarget = Dice(possibleTargets);

    //Get  target
    return diceTarget.Roll();
}

void Game::DoEnnemyTurnUseSpell(bool useObject, shared_ptr<ISpell> spell, shared_ptr<CharacterClass> target)
{
    if (useObject)
    {
        m_fightOrder[0]->UseObject(target.get());
    }
    else //For now, the use skill have only 1 spell, multiple spell choose not implemented now !
    {
        m_fightOrder[0]->UseSkill(target.get());
    }
}

void Game::DoEnnemyTurnMoveLeft()
{
    m_fightingRoom->MoveCharacterOneLane(m_fightOrder[0], false);
    cout << "He moving left" << endl;
}
#pragma endregion Private Method(Enemy Action)

#pragma region Private Method(Menu Action)
void Game::DoActionMenuPass()
{
    //cout << typeid(m_fightOrder[0].get()).name() << " / " << m_fightOrder[0]->GetClassTypeName() << endl;

    //For wizard, necromancer, the pass action regen their mana
    auto isWizard = dynamic_cast<Wizard *>(m_fightOrder[0].get());
    if (isWizard != nullptr)
    {
        isWizard->RegenMana();
    }

    // For slime, he interact with you
    auto isSlime = dynamic_cast<Slime *>(m_fightOrder[0].get());
    if (isSlime != nullptr)
    {
        isSlime->Interact(*m_fightOrder[1]); //No matter with who he interact, just take the next character
    }
}

bool Game::DoActionMenuMove()
{
    //This menu had a GUI, clean terminal before showing it's GUI
    Utility::ClearTerminal(); //Ready for the next GUI
    DisplayFight();

    //Get possible move
    int currentLane = m_fightingRoom->GetLaneOfCharacter(m_fightOrder[0]);

    auto rightMoveAllowed = m_fightingRoom->IsFightMoveAllowed(m_fightOrder[0], static_cast<Room::LaneName>(currentLane + 1)); //right
    auto leftMoveAllowed = m_fightingRoom->IsFightMoveAllowed(m_fightOrder[0], static_cast<Room::LaneName>(currentLane - 1));  //left

    //Prepare the possible Action
    auto menu1Selecter = vector<shared_ptr<MenuItem>>{
        make_shared<MenuItem>(MenuItem("Back to the previous menu", MenuItem::MenuType::Cancel))};

    if (leftMoveAllowed)
        menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Move Left", MenuItem::MenuType::MoveLeft)));
    if (rightMoveAllowed)
        menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Move Right", MenuItem::MenuType::MoveRight)));

    //Set the menu
    string info = "choose your direction";
    if (!rightMoveAllowed && !leftMoveAllowed)
        info = "but he can't move !";
    Menu menu{"Turn of " + m_fightOrder[0]->GetName() + ". Lane Moving, " + info, menu1Selecter};
    menu.DisplayMenu();

    //Selection of the menu
    shared_ptr<MenuItem> selectedMenu{nullptr};
    do
    {
        try
        {
            auto charSelected = menu.AskMenuSelect();
            selectedMenu = menu.SelectMenu(charSelected); //Rise exception if unknow menu
        }
        catch (Ex_MenuItemNotFound const &e)
        {
            cout << "Please, choose a valid action" << endl;
        }
    } while (selectedMenu == nullptr);

    //Do action according the selected menu
    bool hasCancel = false;
    switch (selectedMenu->GetMenuType())
    {
    case MenuItem::MenuType::MoveLeft:
        m_fightingRoom->MoveCharacterOneLane(m_fightOrder[0], false);
        break;
    case MenuItem::MenuType::MoveRight:
        m_fightingRoom->MoveCharacterOneLane(m_fightOrder[0], true);
        break;
    case MenuItem::MenuType::Cancel:
        hasCancel = true;
        break;
    default: //Should not happend
        break;
    }

    return hasCancel;
}

bool Game::DoActionMenuFight()
{
    bool subMenuHasCancel = false;

    //Get the spell/skill
    bool isObjectUsed = false;
    shared_ptr<ISpell> usedSpell = nullptr;
    //tie = unpack tuple https://www.geeksforgeeks.org/returning-multiple-values-from-a-function-using-tuple-and-pair-in-c/
    tie(subMenuHasCancel, isObjectUsed, usedSpell) = DoActionMenuFightSelectSpell();

    //Get the target
    int range = 0;
    if (isObjectUsed) //For now, object have only a range of 1
        range = 1;
    if (usedSpell != nullptr)
        range = usedSpell->GetRange();
    shared_ptr<CharacterClass> target = nullptr;

    if (!subMenuHasCancel)
        tie(subMenuHasCancel, target) = DoActionMenuFightSelectTarget(range);

    //Use the spell/skill
    if (!subMenuHasCancel)
    {
        if (isObjectUsed)
            m_fightOrder[0]->UseObject(target.get());
        else
            m_fightOrder[0]->UseSkill(target.get());
    }

    return subMenuHasCancel;
}

tuple<bool, bool, shared_ptr<ISpell>> Game::DoActionMenuFightSelectSpell()
{
    //This menu had a GUI, clean terminal before showing it's GUI
    Utility::ClearTerminal(); //Ready for the next GUI
    DisplayFight();

    auto menu1Selecter = vector<shared_ptr<MenuItem>>{
        make_shared<MenuItem>(MenuItem("Back to the previous menu", MenuItem::MenuType::Cancel))};

    if (m_fightOrder[0]->GetObject() != nullptr)
        menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Use object", MenuItem::MenuType::FightObject, m_fightOrder[0]->GetObject())));

    // for (auto spell : m_fightOrder[0]->GetSpells())
    //      menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Use Spell " + spell->GetName(), MenuItem::MenuType::FightSpell, spell)));
    menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Use Spell " + m_fightOrder[0]->GetSpells()->GetName(), MenuItem::MenuType::FightSpell, m_fightOrder[0]->GetSpells())));

    //Set the menu
    Menu menu{"Turn of " + m_fightOrder[0]->GetName() + ". Fight, choose your spell", menu1Selecter};
    menu.DisplayMenu();

    //Selection of the menu
    shared_ptr<MenuItem> selectedMenu{nullptr};
    do
    {
        try
        {
            auto charSelected = menu.AskMenuSelect();
            selectedMenu = menu.SelectMenu(charSelected); //Rise exception if unknow menu
        }
        catch (Ex_MenuItemNotFound const &e)
        {
            cout << "Please, choose a valid action" << endl;
        }
    } while (selectedMenu == nullptr);

    //Do action according the selected menu
    bool hasCancel = false;
    bool isObject = false;
    shared_ptr<ISpell> usedSpell = nullptr;
    switch (selectedMenu->GetMenuType())
    {
    case MenuItem::MenuType::FightObject:
        isObject = true;
        break;
    case MenuItem::MenuType::FightSpell:
        usedSpell = static_pointer_cast<ISpell>(selectedMenu->GetLinkedElement());
        break;
    case MenuItem::MenuType::Cancel:
        hasCancel = true;
        break;
    default: //Should not happend
        break;
    }

    return {
        hasCancel, isObject, usedSpell};
}

tuple<bool, shared_ptr<CharacterClass>> Game::DoActionMenuFightSelectTarget(int range)
{
    //This menu had a GUI, clean terminal before showing it's GUI
    Utility::ClearTerminal(); //Ready for the next GUI
    DisplayFight();

    //Build the menu
    auto menu1Selecter = vector<shared_ptr<MenuItem>>{
        make_shared<MenuItem>(MenuItem("Back to the previous menu", MenuItem::MenuType::Cancel))};

    // //Target himself, why not, the user is free of his action !
    //himself is now include in the neighbour loop
    // menu1Selecter.push_back(make_shared<MenuItem>(MenuItem("Himself !", MenuItem::MenuType::CharacterSelection, m_fightOrder[0])));

    //if range = 1 --> from -1 to 1 neighbour
    for (int neighbour = -range; neighbour <= range; ++neighbour)
    {
        auto characters = m_fightingRoom->GetNeighbourCharacters(m_fightOrder[0], neighbour);
        for (auto character : characters)
        {
            string textMenu = character->GetName();
            if (character == m_fightOrder[0])
                textMenu = "Himself !";
            menu1Selecter.push_back(make_shared<MenuItem>(MenuItem(textMenu, MenuItem::MenuType::CharacterSelection, character)));
        }
    }

    //Set the menu
    Menu menu{"Turn of " + m_fightOrder[0]->GetName() + ". Fight, choose your target", menu1Selecter};
    menu.DisplayMenu();

    //Selection of the menu
    shared_ptr<MenuItem> selectedMenu{nullptr};
    do
    {
        try
        {
            auto charSelected = menu.AskMenuSelect();
            selectedMenu = menu.SelectMenu(charSelected); //Rise exception if unknow menu
        }
        catch (Ex_MenuItemNotFound const &e)
        {
            cout << "Please, choose a valid action" << endl;
        }
    } while (selectedMenu == nullptr);

    //Do action according the selected menu
    bool hasCancel = false;
    shared_ptr<CharacterClass> target = nullptr;
    switch (selectedMenu->GetMenuType())
    {
    case MenuItem::MenuType::CharacterSelection:
        target = static_pointer_cast<CharacterClass>(selectedMenu->GetLinkedElement());
        break;
    case MenuItem::MenuType::Cancel:
        hasCancel = true;
        break;
    default: //Should not happend
        break;
    }

    return {hasCancel, target};
}
#pragma endregion Private Method(Menu Action)
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
Game &Game::operator=(const Game &game)
{
    if (this != &game)
    {
        m_nbAlly = game.m_nbAlly;
        m_playerTeam = game.m_playerTeam;
        m_ennemyTeam = game.m_ennemyTeam;
        m_fightOrder = game.m_fightOrder;

        m_gameLevel = game.m_gameLevel;

        //fightingRoom is unique ptr, but this room is created with the teams, just recreate a new room instead a "copy of unique_ptr" !
        if (m_fightingRoom != nullptr)
            m_fightingRoom = make_unique<Room>(Room(m_playerTeam, m_ennemyTeam));
        else
            m_fightingRoom = nullptr;
    }
    return *this;
}
#pragma endregion Member Overload operator