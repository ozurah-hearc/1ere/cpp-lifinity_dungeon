/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   MenuItem.cpp
 * @brief       :   This class is an item of the menu,
 *                  By items, that means an option who will be displayed in the menu
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>

#include "..\..\include\Menu\MenuItem.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
void MenuItem::SetKey(char key)
{
    m_key = key;
}

MenuItem::MenuType MenuItem::GetMenuType() const
{
    return m_type;
}

shared_ptr<void> MenuItem::GetLinkedElement() const
{
    return m_linkedElement;
}

#pragma endregion Getter / Setter

#pragma region Constructor
MenuItem::MenuItem(string text, MenuType type, shared_ptr<void> linkedElement, char key) : m_text(text), m_type(type), m_linkedElement(linkedElement)
{
    SetKey(key);
}

#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
string MenuItem::GetTextMenuItem() const
{
    return string(1, m_key) + ") " + m_text;
}

bool MenuItem::IsThisMenuSelected(char key) const
{
    return toupper(key) == m_key;
}
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator