/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   Menu.cpp
 * @brief       :   This class is menu interface,
 *                  it regroups de functionality to display the menu, and choose an action
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <vector>
#include <algorithm>

#include "..\..\include\Utility.hpp"
#include "..\..\include\Menu\Menu.hpp"
#include "..\..\include\Menu\MenuItem.hpp"
#include "..\..\include\Exception\MenuItemOutOfMenuLimit.hpp"
#include "..\..\include\Exception\MenuItemNotFound.hpp"
#include "..\..\include\Exception\TextOutOfDisplay.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

#pragma endregion Getter / Setter

#pragma region Constructor
Menu::Menu(string title, vector<shared_ptr<MenuItem>> menus, int size, string border)
    : m_title(title), m_menus(menus), m_menuSize(size), m_border(border)
{
    //Can't display more menu than authorized size (limit to use only A-Z keys)
    if (static_cast<int>(menus.size()) >= QTY_MENU_LIMIT)
        throw Ex_MenuItemOutOfMenuLimit();

    char charSelect = 'A';
    for (auto menu : m_menus)
    {
        menu.get()->SetKey(charSelect);
        charSelect++;
    }
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Static Public Method
void Menu::DisplayHomePage()
{
    cout << "###################################################" << endl
         << "#                                                 #" << endl
         << "#  █    ▄█ ▄████  ▄█    ▄   ▄█    ▄▄▄▄▀ ▀▄    ▄   #" << endl
         << "#  █    ██ █▀   ▀ ██     █  ██ ▀▀▀ █      █  █    #" << endl
         << "#  █    ██ █▀▀    ██ ██   █ ██     █       ▀█     #" << endl
         << "#  ███▄ ▐█ █      ▐█ █ █  █ ▐█    █        █      #" << endl
         << "#      ▀ ▐  █      ▐ █  █ █  ▐   ▀       ▄▀       #" << endl
         << "#            ▀       █   ██                       #" << endl
         << "#  ██▄     ▄      ▄     ▄▀  ▄███▄   ████▄    ▄    #" << endl
         << "#  █  █     █      █  ▄▀    █▀   ▀  █   █     █   #" << endl
         << "#  █   █ █   █ ██   █ █ ▀▄  ██▄▄    █   █ ██   █  #" << endl
         << "#  █  █  █   █ █ █  █ █   █ █▄   ▄▀ ▀████ █ █  █  #" << endl
         << "#  ███▀  █▄ ▄█ █  █ █  ███  ▀███▀         █  █ █  #" << endl
         << "#         ▀▀▀  █   ██                     █   ██  #" << endl
         << "#                                                 #" << endl
         << "###################################################" << endl
         << "#               Press Enter to start              #" << endl
         << "###################################################" << endl;
    Utility::WaitEnterForContinue(false);
}

void Menu::DisplayYouDiePage()
{
    cout << "################################################################" << endl
         << "#                                                              #" << endl
         << "#  ▓██   ██▓ ▒█████   █    ██    ▓█████▄  ██▓▓█████     ▐██▌   #" << endl
         << "#   ▒██  ██▒▒██▒  ██▒ ██  ▓██▒   ▒██▀ ██▌▓██▒▓█   ▀     ▐██▌   #" << endl
         << "#    ▒██ ██░▒██░  ██▒▓██  ▒██░   ░██   █▌▒██▒▒███       ▐██▌   #" << endl
         << "#    ░ ▐██▓░▒██   ██░▓▓█  ░██░   ░▓█▄   ▌░██░▒▓█  ▄     ▓██▒   #" << endl
         << "#    ░ ██▒▓░░ ████▓▒░▒▒█████▓    ░▒████▓ ░██░░▒████▒    ▒▄▄    #" << endl
         << "#     ██▒▒▒ ░ ▒░▒░▒░ ░▒▓▒ ▒ ▒     ▒▒▓  ▒ ░▓  ░░ ▒░ ░    ░▀▀▒   #" << endl
         << "#   ▓██ ░▒░   ░ ▒ ▒░ ░░▒░ ░ ░     ░ ▒  ▒  ▒ ░ ░ ░  ░    ░  ░   #" << endl
         << "#   ▒ ▒ ░░  ░ ░ ░ ▒   ░░░ ░ ░     ░ ░  ░  ▒ ░   ░          ░   #" << endl
         << "#   ░ ░         ░ ░     ░           ░     ░     ░  ░    ░      #" << endl
         << "#   ░ ░                           ░                            #" << endl
         << "#                                                              #" << endl
         << "################################################################" << endl
         << "#               Close the game by hitting Enter                #" << endl
         << "################################################################" << endl;
    Utility::WaitEnterForContinue(false);
}
#pragma endregion Static Public Method

#pragma region Public Method
void Menu::DisplayMenu() const
{
    WriteHeader(m_menuSize, m_border, m_title);
    WriteMenusItem(m_menuSize, m_border);
    if (!m_menus.empty()) //Just a menu "header" no need to draw the "bottom line"
        cout << Utility::GetBorder(m_menuSize, m_border) << endl;
}

char Menu::AskMenuSelect() const
{
    char result;
    cout << "Enter your choice (the menu letter) : ";
    cin >> result; // convert the input to a char (take only the first char)

    return result;
}

shared_ptr<MenuItem> Menu::SelectMenu(char key) const
{
    auto element = find_if(m_menus.begin(), m_menus.end(),
                           [&](shared_ptr<MenuItem> const &pmi)
                           { return pmi.get()->IsThisMenuSelected(key); });
    //[](){}  === lambda expression
    //[&] = all the variable used will be used as reference
    //() = naming the value get
    //{} = condition for the lambda
    //https://docs.microsoft.com/en-us/cpp/cpp/lambda-expressions-in-cpp?view=msvc-160

    if (element != m_menus.end())
        return *element;

    else
        throw Ex_MenuItemNotFound(key);
}

#pragma endregion Public Method

#pragma region Private Method
void Menu::WriteMenusItem(int length, string border) const
{
    for (auto menu : m_menus)
    {
        cout << Utility::GetTextLine(length, border, " " + menu.get()->GetTextMenuItem()) << endl;
    }
}

void Menu::WriteHeader(int length, string border, string text) const
{

    cout << Utility::GetBorder(length, border) << endl;
    cout << Utility::GetTextLine(length, border, text, true) << endl;
    cout << Utility::GetBorder(length, border) << endl;
}

#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator