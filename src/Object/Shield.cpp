/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Shield.cpp
 * @brief       :   Class for the shields, contains the stats and capacity of a shield
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Object\Shield.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

int Shield::GetFeature() const
{
    return m_solidity;
}

string Shield::GetName() const
{
    return m_name;
}

#pragma endregion Getter / Setter

#pragma region Constructor

Shield::Shield(string name, int solidity) : m_solidity(solidity)
{
    m_name = name; // not "m_name(name)" cause it's from the abstract class
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
Shield &Shield::operator=(const Shield &shield)
{
    if (this != &shield)
    {
        m_name = shield.m_name;
        m_solidity = shield.m_solidity;
    }
    return *this;
}
#pragma endregion Member Overload operator