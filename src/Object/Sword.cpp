/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   2.0
 * @date        :   09.03.2021
 *
 * @file        :   Sword.cpp
 * @brief       :   Class for the swords, contains the stats and capacity of a sword
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Object\Sword.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

int Sword::GetFeature() const
{
    return m_damage;
}

string Sword::GetName() const
{
    return m_name;
}
#pragma endregion Getter / Setter

#pragma region Constructor

Sword::Sword(string name, int damage) : m_damage(damage)
{
    m_name = name; // not "m_name(name)" cause it's from the abstract class
}
#pragma endregion Constructor

#pragma region Public Method
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
Sword &Sword::operator=(const Sword &sword)
{
    if (this != &sword)
    {
        m_name = sword.m_name;
        m_damage = sword.m_damage;
    }
    return *this;
}
#pragma endregion Member Overload operator