/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Potion.cpp
 * @brief       :   Class for the potions, contains the stats and capacity of a potion
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <string>

#include "..\..\include\Object\IObject.hpp"
#include "..\..\include\Object\Potion.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

int Potion::GetFeature() const
{
    return m_power;
}

string Potion::GetName() const
{
    return m_name;
}

#pragma endregion Getter / Setter

#pragma region Constructor

Potion::Potion(string name, int power) : m_power(power)
{
    m_name = name; // not "m_name(name)" cause it's from the abstract class
}
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Public Method
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator