/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   15.06.2021
 *
 * @file        :   FileManager.hpp
 * @brief       :   This class contains statics methods used for manipulating files (creation, read, write)
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <fstream>


#include "..\include\FileManager.hpp"
#include "..\include\Log\LogCollection.hpp"
#include "..\include\Utility.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter
#pragma endregion Getter / Setter

#pragma region Constructor
#pragma endregion Constructor

#pragma region Destructor
#pragma endregion Destructor

#pragma region Static Public Method
void FileManager::CreateDirectory(string path){
    /*
     with C++ 17 :  // https://docs.w3cub.com/cpp/filesystem/create_directory
     But this solution isn't working for some reason in this program
     (require includes : #include <cstdlib>
                         #include <filesystem>)
     Requier the experimental vesion of C++ to work : https://stackoverflow.com/questions/48729328/link-errors-using-filesystem-members-in-c17
     or using other library, like boost (which is not a part of the standard library condition for this project)
    */ 
    //filesystem::create_directories(path);
    cout << "The create directory system isn't enable for this project, please create manually the directory : \"" << path<< "\" if not already exists" << endl;
    Utility::WaitEnterForContinue(true);
    //throw "Unimplemented function, please create manually the directory !";
}
bool FileManager::IsFileExists(string pathAndFilename)
{
    ifstream test(pathAndFilename);
    return test.good();
    // No need to close the file, it wasn't opened
}

bool FileManager::WriteInFile(string pathAndFilename, string data, bool addAtEnd)
{
    bool result{false};

    std::ofstream outputFile;
    auto mode = ios::trunc;
    if (addAtEnd)
        mode = ios::app;

    outputFile.open(pathAndFilename, std::ios::out | mode);

    //Error when opening the file
    if (!outputFile.fail())
    {
        //Write in file
        outputFile << data << endl;
        result = true;
        outputFile.close();
    }

    return result;
}

bool FileManager::ConfirmBeforeOverwriteFile(string pathAndFilename)
{
    bool allowedToCreate{true};
    if (IsFileExists(pathAndFilename))
    {
        char confirm;
        cout << "The file already exists, did you want to overwrite it ? ('y' others cancel) :";
        cin >> confirm; // Get the first character

        // Allows the "o" for habitude of the "yes" in french
        allowedToCreate = toupper(confirm) == 'O' || toupper(confirm) == 'Y';
    }

    return allowedToCreate;
}

bool FileManager::WriteInFileWithConfirmOverwrite(string pathAndFilename, string data)
{
    bool result{false};

    if (ConfirmBeforeOverwriteFile(pathAndFilename))
        result = WriteInFile(pathAndFilename, data, false);

    return result;
}

#pragma endregion Static Public Method

#pragma region Public Method
#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator