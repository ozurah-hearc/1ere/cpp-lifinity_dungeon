/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   LogCollection.cpp
 * @brief       :   Class for the centralisation of the logs. In other words, this class contains each logs who occurs during the execution of the program
 *                  It contains also the ability for saving the log in files
 * @remark      :   This class is an singleton, that's means it can have only 1 instance of this class
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>
#include <chrono>
#include <ctime> //For date string

#include "..\..\include\Log\LogCollection.hpp"
#include "..\..\include\Log\Log.hpp"
#include "..\..\include\Utility.hpp"
#include "..\..\include\FileManager.hpp"
#include "..\..\include\Exception\LogCantBeWrite.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Variable Init
LogCollection *LogCollection::m_instance = nullptr;
#pragma endregion Variable Init

#pragma region Getter / Setter
void LogCollection::SetAutoSave(bool enable)
{
    m_autosave = enable;
}

void LogCollection::SetName(string name)
{
    if (m_namePrecededByDate)
    {
        auto dt = Utility::GetDateTimeFromSysClock(std::chrono::system_clock::now());
        m_saveName = Utility::dtTupleToString(dt) + "_" + name;
    }
    else
        m_saveName = name;
}

string LogCollection::GetPathAndFileName()
{
    return m_saveFolderPath + m_saveName;
}

#pragma endregion Getter / Setter

#pragma region Constructor

LogCollection::LogCollection(string path, string name, bool dateBeforeName, bool autosave)
    : m_saveFolderPath(path), m_saveName(name), m_namePrecededByDate(dateBeforeName), m_autosave(autosave)
{
    SetName(name);
    FileManager::CreateDirectory(path);
}
LogCollection::LogCollection()
{
    SetName(m_saveName); // Update with time
    FileManager::CreateDirectory(m_saveFolderPath);
}
#pragma endregion Constructor

#pragma region Destructor
LogCollection::~LogCollection()
{
    m_instance = nullptr;
}
#pragma endregion Destructor

#pragma region Public Method
#pragma region Static
LogCollection *LogCollection::InitLogCol(string path, string name, bool dateBeforeName, bool autosave)
{
    if (m_instance == nullptr)
    {
        m_instance = new LogCollection(path, name, dateBeforeName, autosave);
    }
    else
    {
        m_instance->m_saveFolderPath = path;
        m_instance->m_namePrecededByDate = dateBeforeName;
        m_instance->m_autosave = autosave;
        m_instance->SetName(name);
    }
    return m_instance;
}

LogCollection *LogCollection::GetInstance()
{
    if (m_instance == nullptr)
    {
        m_instance = new LogCollection();
    }
    return m_instance;
}
#pragma endregion Static

void LogCollection::SaveLog()
{
    while (!m_unsavedLogs.empty())
    {
        AddHeaderInLogFile(GetPathAndFileName());

        auto element = move(m_unsavedLogs.front());

        //Write in file
        auto logString = element->ToString();

        bool result = FileManager::WriteInFile(GetPathAndFileName(), logString);

        // If the file can't be  created, it will try to write to another file to keep the log even if the program crash
        if (!result)
        {
            bool tmpDisableAutoSave = m_autosave;
            m_autosave = false;

            AddLog(make_unique<Log>(Log{Log::LogLevel::Critical, Log::LogType::File, "The file can't be written !"}));

            string newFileName = m_saveFolderPath + "FileSaveError_" + m_saveName;
            bool resultAutoSave = FileManager::WriteInFile(newFileName, logString);
            // The log can't be saved, this is really dangerous; throw an exception !
            if (!resultAutoSave)
            {
                AddHeaderInLogFile(newFileName);
                AddLog(make_unique<Log>(Log{Log::LogLevel::Critical, Log::LogType::File, "Second attempt to write, the file is still unwritten !"}));
                throw Ex_LogCantBeWrite(element.get(), "The log can't be write, the file is maybe unacessible");
            }

            m_autosave = tmpDisableAutoSave;
        }

        //Change from unsaved to saved
        m_unsavedLogs.pop();
        m_savedLogs.push(move(element));
    }
}
void LogCollection::AddLog(unique_ptr<Log> log)
{
    m_unsavedLogs.push(move(log));
    if (m_autosave)
        SaveLog();
}

#pragma endregion Public Method

#pragma region Private Method
void LogCollection::AddHeaderInLogFile(string pathAndFilename)
{
    if (!FileManager::IsFileExists(pathAndFilename))
    {
        FileManager::WriteInFile(pathAndFilename, "Date\tLevel\tType\tMessage");
    }
}

#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator
