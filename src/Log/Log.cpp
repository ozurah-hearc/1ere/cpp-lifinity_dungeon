/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Log.cpp
 * @brief       :   Class for the entry of logs, an entry is caracterised by the level of the log; the type; and the message; it contains also the datetime of when the log occurs
 * 
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <memory>
#include <string>
#include <chrono>

#include "..\..\include\Log\Log.hpp"
#include "..\..\include\Utility.hpp"

using namespace std;
using namespace He_Arc::RPG;

#pragma region Getter / Setter

Log::LogLevel Log::GetLogLevel() const
{
    return m_logLevel;
}
Log::LogType Log::GetLogType() const
{
    return m_logType;
}
chrono::system_clock::time_point Log::GetLogTime() const
{
    return m_time;
}
string Log::GetLogMessage() const
{
    return m_message;
}
#pragma endregion Getter / Setter

#pragma region Constructor
Log::Log(LogLevel level, LogType type, string message)
    : m_logLevel(level), m_logType(type), m_message(message)
{
}
#pragma endregion Constructor

#pragma region Destructor

#pragma endregion Destructor

#pragma region Public Method
string Log::ToString() // Due to use the map converter, can't be setted as const // source : https://stackoverflow.com/questions/262853/c-map-access-discards-qualifiers-const
{
    return Utility::dtTupleToString(Utility::GetDateTimeFromSysClock(m_time)) +
           "\t" + LogLevelToString[m_logLevel] +
           "\t" + LogTypeToString[m_logType] +
           "\t" + m_message;
}

#pragma endregion Public Method

#pragma region Private Method
#pragma endregion Private Method

#pragma region Not - Member Overload operator
#pragma endregion Not - Member Overload operator

#pragma region Member Overload operator
#pragma endregion Member Overload operator
