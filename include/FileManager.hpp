/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   15.06.2021
 *
 * @file        :   FileManager.hpp
 * @brief       :   Header file, for the class "FileManager.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>


using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class contains statics methods used for manipulating files (creation, read, write)
     * 
     */
    class FileManager
    {
    private:
        /**
         * @brief Private constructor, this class contains only static methods, there is no reason to allows to create an object on this
         * 
         */
        FileManager() = default;

    public:
        virtual ~FileManager() = default; //Make the class abstract

        /**
         * @brief Verifiy if the file already exists
         * 
         * @param pathAndFilename Path and filename(with extension) of the file to check
         * @return true The file exists
         * @return false The file doesn't exists
         */
        static bool IsFileExists(string pathAndFilename);
        /**
         * @brief Create the directory if didn't exists
         * 
         * @param path path of the directory to create
         * @remark This function isn't implemented (require experimental c++)
         *         So, instead of creaty dynamiclly the directory, ask to the user to do it with an "wait enter for continue"
         */
        static void CreateDirectory(string path);

        /**
         * @brief If the file exists, prompt a question to the user if he want to overwrite the file
         * 
         * @param pathAndFilename Path and filename(with extension) of the file to check
         * @return true The file can be overwritten if the user answer is "y" (or "o" for French speaking)
         * @return false The file can't be overwritten (if the user answer is anything else)
         */
        static bool ConfirmBeforeOverwriteFile(string pathAndFilename);
        /**
         * @brief Write the text in the file
         * 
         * @param pathAndFilename Path and filename(with extension) of the file where write the data
         * @param data The text (data) to write in the file
         * @param addAtEnd True : The data will be add at the end of the file
         *                 False : The file will be overwritten and write the new data in
         * @return true The text is fully written
         * @return false Some errors occurs when writing, the file can have missing data or was not written
         */
        static bool WriteInFile(string pathAndFilename, string data, bool addAtEnd = true);
        /**
         * @brief Prompt a query to be sure if the file can be overwritten if already exists
         *          If result yes : overwritte the file
         *          If result no : the file isn't overwritte
         * 
* @param pathAndFilename Path and filename(with extension) of the file where write the data
         * @param data The text (data) to write in the file
         * @return true The file was overwritten and have now the new data
         * @return false The file wasn't overwritten and have the old data
         */
        static bool WriteInFileWithConfirmOverwrite(string pathAndFilename, string data);
    };
} // namespace He_Arc::RPG
