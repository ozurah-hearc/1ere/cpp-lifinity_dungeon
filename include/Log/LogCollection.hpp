/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   LogCollection.hpp
 * @brief       :   Header file, for the class "LogCollection.cpp"
 * @remark      :   This class is a singleton
 *
 * @copyright Copyright (c) 2021
 * 
 * @remark : Source tuto of the singleton : https://refactoring.guru/fr/design-patterns/singleton/cpp/example#example-0
 *                                          (exemple 0, this version of singleton isn't thread safe !)
 */

#pragma once

#include <iostream>
#include <memory>
#include <queue>
#include "./Log.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief The LogCollection is a class for the centralisation of the logs. In other words, this class contains each logs who occurs during the execution of the program
    *         It contains also the ability for saving the log in files
     */
    class LogCollection
    {
    protected:
        /**
         * @brief reference on himself
         * 
         */
        static LogCollection *m_instance;

        /**
         * @brief Ordered collection of each logs who occurs and was already saved in the file
         *          (The collection is of type "FIFO (First in, first out)")
         * 
         */
        queue<unique_ptr<Log>> m_savedLogs;
        /**
         * @brief Ordered collection of each logs who occurs and was already saved in the file
         *          (The collection is of type "FIFO (First in, first out)")
         * 
         */
        queue<unique_ptr<Log>> m_unsavedLogs;
        /**
         * @brief The path where the logs will be saved (folder)
         * 
         */
        string m_saveFolderPath{"./log/"};
        /**
         * @brief The filename (with extension) where the logs will be saved
         * 
         */
        string m_saveName{"logs.log"};
        /**
         * @brief is the filename must be overwriten with the date before ?
         * @remarks this condition is applied  when calling "SetName()"
         * 
         */
        bool m_namePrecededByDate{true};
        /**
         * @brief Is the logs are saved after each add (True), or saved only when asked (False) ?
         * 
         */
        bool m_autosave{true};

        /**
         * @brief Construct a new Log Collection object
         * @remark This constructor is private, because a singleton define the method 
         *          "GetInstance()" to be sure of having only 1 instance of the class
         * 
         * @param path Where the logs will be saved (folder)
         * @param name Where the logs will be saved (filename with extension)
         * @param dateBeforeName Is the name is preceded by the date (when the log name is setted)
         * @param autosave Is the log are autosaved after each add
         */
        LogCollection(string path, string name, bool dateBeforeName, bool autosave);
        LogCollection();
        /**
         * @brief Set the name of the file
         *        if "m_namePrecededByDate" is true, the name will be preceded of the current datetime
         * 
         * @param name 
         */
        void SetName(string name);

        /**
         * @brief Get the Path And Filename where the file will be write
         * 
         * @return string A concatenation of "m_saveFolderPath" and "m_saveFolderPath"
         */
        string GetPathAndFileName();

        /**
         * @brief Add into the file the header row for the logs (only if the file doesn't exists)
         * 
         * @param pathAndFilename File to check if exist, and if not, add the header log
         */
        void AddHeaderInLogFile(string pathAndFilename);

    public:
        /**
         * @remark Delete the cloneable ability, because a singleton should not be cloneable
         * 
         */
        LogCollection(LogCollection &) = delete;
        /**
         * @remark Delete the assignation ability, because a singleton should not be assignable
         * 
         */
        void operator=(const LogCollection &) = delete;
        /**
         * @brief Destroy the Log Collection object
         * @remark Didn't set to default, because contains a pointer (the pointer on himself)
         */
        virtual ~LogCollection();

        /**
         * @brief Get the instance of the LogCollection with setting the options of the log system
         * 
         * @param path Where the logs will be saved (folder)
         * @param name Where the logs will be saved (filename with extension)
         * @param dateBeforeName Is the name is preceded by the date (when the log name is setted)
         * @param autosave Is the log are autosaved after each add
         * @return LogCollection* The single instance of the log
         */
        static LogCollection *InitLogCol(string path, string name, bool dateBeforeName, bool autosave);

        /**
         * @brief Get the Instance of the LogCollection
         * 
         * @return LogCollection* The single instance of the log 
         */
        static LogCollection *GetInstance();

        /**
         * @brief Save the "unsaved" logs to the file and mark them as "saved"
         * 
         */
        void SaveLog();
        /**
         * @brief Add a log to the collection
         * @remark if "autosave" is True, this function call "SaveLog()" too
         * 
         * @param log log to add at the "unsaved log"
         */
        void AddLog(unique_ptr<Log> log);
        /**
         * @brief Set the AutoSave state for the logs
         *        If autosave = true, the log will be saved in the file when they was add to the LogCollection
         *        If autosave = false, the log will be saved only when the method "SaveLog()" is call
         * 
         * @param enable Enable the auto save option ?
         */
        void SetAutoSave(bool enable);
    };
} // namespace He_Arc::RPG
