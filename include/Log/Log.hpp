/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Log.hpp
 * @brief       :   Header file, for the class "Log.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <map>
#include <chrono>

#include "./Log.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class is an entry of logs, an entry is caracterised by the level of the log; the type; and the message; it contains also the datetime of when the log occurs
     */
    class Log
    {
    public:
        /**
         * @brief The level of the log (is used when)
         */
        enum LogLevel
        {
            Default,
            Debug,
            Game,
            Critical,
        };

        /**
         * @brief Convert a LogLevel value to the corresponding string value
         * 
         */
        map<LogLevel, string> LogLevelToString = {
            {Default, "Default"},
            {Debug, "Debug"},
            {Game, "Game"},
            {Critical, "Critical"}
        };

        /**
         * @brief The type of the log (what have did the log)
         */
        enum LogType
        {
            MenuAction,
            Character,
            Move,
            Fight,
            Dice,
            File,
            Room,
            Skill,
            Other,
        };

        /**
         * @brief Convert a LogType value to the corresponding string value
         * 
         */
        map<LogType, string> LogTypeToString = {
            {MenuAction, "Menu Action"},
            {Character, "Character"},
            {Move, "Movement"},
            {Fight, "Fighting"},
            {Dice, "Dice"},
            {File, "File"},
            {Room, "Room"},
            {Other, "Other"},
        };

    protected:
        /**
         * @brief The level of the log
         * 
         */
        LogLevel m_logLevel{LogLevel::Default};
        /**
         * @brief The type of the log
         * 
         */
        LogType m_logType{LogType::Other};
        /**
         * @brief The time when the log occurs
         * 
         */
        chrono::system_clock::time_point m_time{chrono::system_clock::now()};
        /**
         * @brief The message (description) of the log
         * 
         */
        string m_message{""};

    public:
        /**
         * @brief Construct a new Log object
         * 
         * @param level Level of the log
         * @param type Type of the log
         * @param message Message (description) of the log
         */
        Log(LogLevel level, LogType type, string message);
        virtual ~Log() = default;

        /**
         * @brief Get the Level of the log
         * 
         * @return LogLevel level of the log
         */
        LogLevel GetLogLevel() const;
        /**
         * @brief Get the Type of the log
         * 
         * @return LogType type of the log
         */
        LogType GetLogType() const;
        /**
         * @brief Get the time of when the log occurs
         * 
         * @return chrono::system_clock::time_point the time of the log
         */
        chrono::system_clock::time_point GetLogTime() const;
        /**
         * @brief Get the message (description) of the log
         * 
         * @return string message of the log
         */
        string GetLogMessage() const;
        /**
         * @brief Get an string which one contains each information of the log
         * 
         * @return string the log string in the format "[date]\t[level]\t[type]\t[message]"
         */
        string ToString();
    };
} // namespace He_Arc::RPG
