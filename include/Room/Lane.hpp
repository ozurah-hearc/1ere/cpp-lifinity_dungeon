/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   25.05.2021
 *
 * @file        :   Lane.hpp
 * @brief       :   Header file, for the class "Lane.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <list>
#include <queue>
#include "..\Enums.hpp"
#include "..\Character\CharacterClass.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief A lane is a part of a room, one lane have 0-N characters on it.
     *        It can be compared to the "cell" of the room
     *        Only 1 team at once can go on a lane
     * 
     */
    class Lane
    {
    private:
        /**
         * @brief number of space (left and right) when displaying the name in the lane
         */
        const int DISPLAY_CHARACTERNAME_SPACING{2 * 2}; //*2 for left and right
        /**
         * @brief Minimal width (without border) of the "graphical" display mode of the lane
         */
        const int MIN_DISPLAY_WIDTH{DISPLAY_CHARACTERNAME_SPACING + 1}; // left center (1) left
        /**
         * @brief Minimal height (without border) of the "graphical" display mode of the lane
         */
        const int MIN_DISPLAY_HEIGHT{1};

        /**
         * @brief Name of the lane (to indentify it)
         * 
         */
        string m_name{"Unnamed"};
        /**
         * @brief The team who owned this lane
         * 
         */
        TeamGrp m_owner{TeamGrp::None};
        /**
         * @brief Characters present on this lane
         * 
         */
        vector<shared_ptr<CharacterClass>> m_characters;

    public:
        /**
         * @brief Construct a new Lane object
         * 
         * @param name the name of the lane
         */
        Lane(string name = "Unnamed");
        virtual ~Lane() = default;
        /**
         * @brief Is the lane has character ?
         * 
         * @return true 1 or more character is on the lane
         * @return false 0 character is on the lane
         */
        bool IsLaneEmpty() const;
        /**
         * @brief Is the character is present on this lane ?
         * 
         * @param character Character to check if is on the lane
         * @return true The character is on this lane
         * @return false The character isn't on this lane
         */
        bool IsCharacterOn(const shared_ptr<CharacterClass> &character) const;
        /**
         * @brief Get the number of character present on this lane
         * 
         * @return int Quantity of character on the lane
         */
        int GetNbCharacterOnTheRoom() const;
        /**
         * @brief Add a character on the lane if the lane is not belong to anyone or already at the good team
         *        If the lane is not belong to anyone, the team becomes the owner
         * 
         * @param character character to add on the lane
         * @param team character's team
         * @return true if the character was add on the lane
         * @return false if the character can be add on the lane (lane owned by another team)
         */
        bool AddCharacter(shared_ptr<CharacterClass> character, TeamGrp team);
        /**
         * @brief Remove the character from the lane
         *        if after removing, there is no more character to the lane, the owner is setted to none team's
         * 
         * @param character character to remove
         */
        void RemoveCharacter(const shared_ptr<CharacterClass> &character);
        /**
         * @brief Get the information if a team can go on this lane
         * 
         * @return true if the lane is owned by noone or by the team
         * @return false if the lane is owned by another team
         */
        bool IsLaneAvailableFor(const TeamGrp team) const;
        /**
         * @brief Get the characters presents on the lane
         * 
         * @return vector<shared_ptr<CharacterClass>> All of the characters present on the lane
         */
        vector<shared_ptr<CharacterClass>> GetCharactersOnLane() const;
        /**
         * @brief Get the owner of the lane
         * 
         * @return TeamGrp owner of the lane
         */
        TeamGrp GetOwner() const;
        /**
         * @brief Display the lane info : the owner, the characters on it
         * 
         */
        void ShowLaneInfo() const;
        /**
         * @brief Display the lane info : the owner, the characters on it
         *        (the output is more condensed than ShowLaneInfo())
         *        and allow a shifting for more esthetic
         * 
         * @param nbShift Left shifting of the output
         */
        void ShowLaneInfoCondensed(int nbShift = 0) const;

        /**
         * @brief Get the sequence for displaying the lane "graphically"
         * 
         * @param height Height of the lane (number of row, without count the top and bottom line).
         * The lane will always been displayed 
         * with at least with an height of the number of character on the lane
         * The minimal row is "MIN_DISPLAY_HEIGHT"
         * 
         * @return queue<string> a "collection" of string ready to use for displaying the lane
         *                       each "row" of the collection is one line for the display
         * @remark return a collection of string instead of use cout, to allow displaying multiple lane side by side
         */
        queue<string> GetSequenceDisplayLane(int height = 0) const;
    };

} // namespace He_Arc::RPG
