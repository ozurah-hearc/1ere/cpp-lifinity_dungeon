/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   25.05.2021
 *
 * @file        :   Room.hpp
 * @brief       :   Header file, for the class "Room.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include ".\Lane.hpp"
#include "..\Enums.hpp"
#include "..\Character\CharacterClass.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief The room is where the characters can do a fight.
     *        It contains lanes, and handle the laning (move character on lanes)
     *        The room also contains the team configuration infos
     * 
     */
    class Room
    {
    public:
        /**
         * @brief The Start index of fighting lanes
         */
        const int FIRST_FIGHT_LANE = 1;
        /**
         * @brief The quantity of fighting lanes
         * @remark the size is linked to the number of positive (>0) value in LaneName Enum
         */
        const int ROOM_FIGHT_LANE_SIZE = 6;

        /**
         * @brief The name of each possible lane in the room
         * @remark if add new lanes, modify variable ROOM_FIGHT_LANE_SIZE and FIRST_FIGHT_LANE
         */
        enum LaneName
        {
            BackLaneLeftSide = 1,
            MidLaneLeftSide,
            FrontLaneLeftSide,
            FrontLaneRightSide,
            MidLaneRightSide,
            BackLaneRightSide,
            DeadRightSide = -1, // ally team dead characters
            DeadLeftSide = -2,  // enemy team dead characters
            None = -3           // Not on a lane (default, no utility now)
        };

    private:
        /**
         * @brief The fightings lanes (where the characters can move)
         * @remark didn't include dead lanes
         */
        vector<shared_ptr<Lane>> m_fightLanes;
        /**
         * @brief The dead lanes (where the characters is placed when their are dead)
         * 
         */
        vector<shared_ptr<Lane>> m_deadLanes;
        /**
         * @brief The first lane (from the left)
         *          (the backest left lane)
         * 
         */
        shared_ptr<Lane> m_backLaneLeftSide = make_shared<Lane>(Lane{"Back, left side"});
        /**
         * @brief The second lane (from the left)
         *          (the left mid lane)
         */
        shared_ptr<Lane> m_midLaneLeftSide = make_shared<Lane>(Lane{"Mid, left side"});
        /**
         * @brief The third lane (from the left)
         *          (the frontest left lane)
         * 
         */
        shared_ptr<Lane> m_frontLaneLeftSide = make_shared<Lane>(Lane{"Front, left side"});
        /**
         * @brief The fourth lane (from the left)
         *          (the fontest right lane)
         */
        shared_ptr<Lane> m_frontLaneRightSide = make_shared<Lane>(Lane{"Front, Right side"});
        /**
         * @brief The fifth lane (from the left)
         *          (the right mid lane)
         */
        shared_ptr<Lane> m_midLaneRightSide = make_shared<Lane>(Lane{"Mid, Right side"});
        /**
         * @brief The sixth lane (from the left)
         *          (the backest right lane)
         */
        shared_ptr<Lane> m_backLaneRightSide = make_shared<Lane>(Lane{"Back, Right side"});
        /**
         * @brief The left dead lane, which one contains dead characters of the left team
         * 
         */
        shared_ptr<Lane> m_deadLaneLeftSide = make_shared<Lane>(Lane{"Ally team dead characters"});
        /**
         * @brief The right dead lane, which one contains dead characters of the righ team
         * 
         */
        shared_ptr<Lane> m_deadLaneRightSide = make_shared<Lane>(Lane{"Enemy team dead characters"});

        /**
         * @brief The team 1 (team on the left side)
         * 
         */
        vector<shared_ptr<CharacterClass>> m_team1;
        /**
         * @brief The team 2 (team on the right side)
         * 
         */
        vector<shared_ptr<CharacterClass>> m_team2;

        /**
         * @brief Get the All lanes merged into 1 vector (fight lanes, dead lanes, ...)
         * 
         * @return vector<shared_ptr<Lane>> all lanes of the room (fight lanes, dead lanes, ...)
         */
        vector<shared_ptr<Lane>> GetAllLanes() const;

        /**
         * @brief Place randomly the characters on the lanes, team 1 on the left side, team 2 on the right side
         * 
         */
        void RandomPlaceCharactersOnLanes();

        /**
         * @brief is the character can move on this lane (only use for moving between "fighting lane") (have an int to LaneName verification cast)
         * 
         * @param character Character to move
         * @param dest Lane where the character go (int value of the LaneName)
         * @return true The character can move on the lane
         * @return false The character can't move on the lane
         */
        bool IsFightMoveAllowed(shared_ptr<CharacterClass> character, int dest);
        /**
         * @brief Is the lane is used as fight lane
         * 
         * @param lane Lane to verifiy
         * @return true It's a fight lane
         * @return false It's other lane (like dead lane)
         */
        bool IsFightLane(const LaneName lane) const;
        /**
         * @brief Is the lane is used as fight lane
         * @remark int value comes from "LaneName" cast
         * 
         * @param lane Lane to verifiy
         * @return true It's a fight lane
         * @return false It's other lane (like dead lane)
         */
        bool IsFightLane(const int lane) const;

    public:
        /**
         * @brief Construct a new Room object
         * 
         * @param team1 Team who will be placed on the left side
         * @param team2 Team who will be placed on the right side
         */
        Room(vector<shared_ptr<CharacterClass>> team1, vector<shared_ptr<CharacterClass>> team2);
        virtual ~Room() = default;
        /**
         * @brief is the character can move on this lane (only use for moving between "fighting lane"
         * 
         * @param character Character to move
         * @param dest Lane where the character go
         * @return true The character can move on the lane
         * @return false The character can't move on the lane
         */
        bool IsFightMoveAllowed(shared_ptr<CharacterClass> character, LaneName dest);
        /**
         * @brief is the character can move on this lane
         * 
         * @param character Character to move
         * @param dest Lane where the character go
         * @return true The character can move on the lane
         * @return false The character can't move on the lane
         */
        bool IsMoveAllowed(shared_ptr<CharacterClass> character, LaneName dest);
        /**
         * @brief Move the character on the lane to their right or left
         * 
         * @param character character to move
         * @param isRightMove true : Move to the right, false : Move to the left (only for moving in fight lane)
         * 
         * @return true if the move is success
         * @return false if the move fail (character is not on a "fight" lane, or the move is out of "fight" lanes of the room or the lane is possessed by other team)
         */
        bool MoveCharacterOneLane(shared_ptr<CharacterClass> character, bool isRightMove);
        /**
         * @brief Move the character to the specified lane
         * 
         * @param character character to move
         * @param destination Lane where the character will be
         *
         * @return true if the move is success
         * @return false if the move fail if the lane is possessed by other team)
         */
        bool MoveCharacterToLane(shared_ptr<CharacterClass> character, LaneName destination);
        /**
         * @brief Move the character to the deadlane of his team
         * 
         * @param character Character to move
         */
        void MoveToDeadLane(shared_ptr<CharacterClass> character);
        /**
         * @brief Get the Lane where the character is actually
         * 
         * @param character character to get the lane
         * @return Lane the lane where the character is
         */
        LaneName GetLaneOfCharacter(const shared_ptr<CharacterClass> &character);
        /**
         * @brief Get the Characters present on the lane
         * 
         * @param LaneName Lane to get
         * @param onlyFightLane Is the method need to check if lanes are used for fighting
         * @param specifiedTeam get only characters if they was in the specified team
         *                      If "None" : get all characters (no matter the team)
         * @return vector<shared_ptr<CharacterClass>> Characters on the specified lane
         */
        vector<shared_ptr<CharacterClass>> GetCharactersOnLane(const LaneName laneName, bool onlyFightLane, TeamGrp specifiedTeam = TeamGrp::None);
        /**
         * @brief Get the Neighbour of the character (characters near the "source")
         *          If "from" is on left middle lane; the "direction" is 1; it will return the characters on the left front lane
         * @remark get only neighbour in the fighting lanes
         * 
         * @param source character to get neighbour
         * @param direction Neighbour at which position from the "source"
         * @param specifiedTeam get only characters if they was in the specified team
         *                      If "None" : get all characters (no matter the team)
         *  @return vector<shared_ptr<CharacterClass>> Neighbour of the character at the distance of "neighbourDirection" 
         */
        vector<shared_ptr<CharacterClass>> GetNeighbourCharacters(const shared_ptr<CharacterClass> source, int direction, TeamGrp specifiedTeam = TeamGrp::None);
        /**
         * @brief Get the Lane where the character is actually
         * 
         * @param character character to get the lane
         * @return Lane the lane where the character is
         */
        TeamGrp GetTeamOfCharacter(const shared_ptr<CharacterClass> &character);

        /**
         * @brief Display the lane info : the teams, the lanes info
         * 
         */
        void ShowRoomInfo() const;

        /**
         * @brief Display the room "graphically"
         * 
         */
        void DisplayRoom() const;

        /**
         * @brief Convert a LaneName value to the corresponding lane object
         * 
         */
        map<LaneName, shared_ptr<Lane>> LaneNameToLane = {
            {LaneName::BackLaneLeftSide, m_backLaneLeftSide},
            {LaneName::MidLaneLeftSide, m_midLaneLeftSide},
            {LaneName::FrontLaneLeftSide, m_frontLaneLeftSide},
            {LaneName::FrontLaneRightSide, m_frontLaneRightSide},
            {LaneName::MidLaneRightSide, m_midLaneRightSide},
            {LaneName::BackLaneRightSide, m_backLaneRightSide},
            {LaneName::DeadLeftSide, m_deadLaneLeftSide},
            {LaneName::DeadRightSide, m_deadLaneRightSide},
            {LaneName::None, nullptr}};

        /**
         * @brief Convert a lane object to the corresponding LaneName value
         * 
         */
        map<shared_ptr<Lane>, LaneName> LaneToLaneName = {
            {m_backLaneLeftSide, LaneName::BackLaneLeftSide},
            {m_midLaneLeftSide, LaneName::MidLaneLeftSide},
            {m_frontLaneLeftSide, LaneName::FrontLaneLeftSide},
            {m_frontLaneRightSide, LaneName::FrontLaneRightSide},
            {m_midLaneRightSide, LaneName::MidLaneRightSide},
            {m_backLaneRightSide, LaneName::BackLaneRightSide},
            {m_deadLaneLeftSide, LaneName::DeadLeftSide},
            {m_deadLaneRightSide, LaneName::DeadRightSide},
            {nullptr, LaneName::None}};
    };

} // namespace He_Arc::RPG

/* OLD CODE

        /-**
         * @brief Linking between a team and the TeamGrp
         *
         *-/
        map<vector<shared_ptr<CharacterClass>> , TeamGrp> m_teamGrp = {
            {m_team1, TeamGrp::Team1},
            {m_team2, TeamGrp::Team2}};

        /-**
         * @brief Get the lane corresponding to the linked enum
         * 
         * @param lane lane name to get
         * @return shared_ptr<Lane> corresponding lane
         *-/
        shared_ptr<Lane> GetLane(const LaneName lane);

        /-**
         * @brief Get the LaneName value corresponding to the linked variable
         * 
         * @param lane variable of the lane
         * @return LaneName LaneName corresponding
         *-/
        LaneName GetLaneName(const shared_ptr<Lane> lane) const;

*/