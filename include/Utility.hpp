/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   Utility.hpp
 * @brief       :   Header file, for the class "Utility.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <sstream> // stringstream
#include <iomanip> // padding setw
#include <tuple>
#include <chrono>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class contains statics methods qualified as utility (not specific to 1 class)
     * 
     */
    class Utility
    {
    private:
        /**
         * @brief Nb of blanc line who will be add when clearing the screen ("fake clean method") (when calling ClearTerminal)
         * 
         */
        static const int NB_LINE_CLEARING_SCREEN{20};
        /**
         * @brief Private constructor, this class contains only static methods, there is no reason to allows to create an object on this
         * 
         */
        Utility() = default;

    public:
        typedef tuple<int, int, int, int, int, int> dtTuple;
        virtual ~Utility() = default; //Make the class abstract
        /**
         * @brief Clear the terminal display
         * @remark Actually, the clear is only add empty line X times
         *          Didn't use any terminal clear (not found, except with lose OS compability)
         * 
         */
        static void ClearTerminal();
        /**
         * @brief Wait for the users press enter in the terminal
         * 
         * @param showMsg if true, display the message "press enter to continue"
         */
        static void WaitEnterForContinue(bool showMsg = true);
        /**
         * @brief Add space at the left of the text, to spacfor padding the text
         * 
         * @param totalLength Total Length of the sequence (if text is 3 char, and total len is 10, add 7 space)
         * @param text Text to add left padding
         * @return string Text with left padding
         */
        static string LeftPadding(int totalLength, string text);
        /**
         * @brief Add space at the left of the text, to spacfor padding the text
         * 
         * @param totalLength Total Length of the sequence (total len is 10, add 9 space)
         * @param character character to add left padding
         * @return string character with left padding
         */
        static string LeftPadding(int totalLength, char character);
        /**
         * @brief Create a string of "qty" tab (\t)
         * 
         * @param qty Quantity of \t
         * @return string the string with \t
         */
        static string TabShift(int qty);
        /**
         * @brief Calculate the padding left to center the text between a length
         *          Only the space in the left are added
         * 
         * @param length Total length of the sequencence
         * @param text Text to center
         * @return string text centered with space on the left
         */
        static string CenterTextWithSpace(int length, string text);
        /**
         * @brief Get a horizontal border (line)
         * 
         * @param length length (nb of character) of the border
         * @param border character(s) used to do the border  (if more than 1 character, it would be count as 1 according to the lenght)
         * @remark use a string to be more flexing with the type of border and allow to use special UTF8 character
         * @return string the border
         */
        static string GetBorder(int length, string border);
        /**
         * @brief Get the string of a line with left and right (like : -   text   - )
         * 
         * @param length Size (in character) of the menu (left and right border included)
         * @param border Character(s) used to draw the border of the menu
         * @param text Text to write
         * @param center True : the text will be centered
         *               False : the text will be left aligned
         * 
         * @return The line of the text, with left and right border
         */
        static string GetTextLine(int length, string border, string text, bool center = false);
        /**
         * @brief Get the Date and the Time From chrono System_clock
         * 
         * @param clock The source of the date and time
         * @return tuple<int, int, int, int, int, int> 1st : year
         *                                             2nd : month
         *                                             3nd : day
         *                                             4nd : hour
         *                                             5nd : min
         *                                             6nd : sec
         */
        static dtTuple GetDateTimeFromSysClock(const chrono::system_clock::time_point clock);
        /**
         * @brief Get a string of the dtTuple (format will be YY-MM-DD_HH-MM-SS)
         * 
         * @param dt the date time tuple to convert
         * @return string the string of the date and time
         */
        static string dtTupleToString(dtTuple dt);
        /**
         * @brief Convert a vector to list
         * 
         * @tparam T The type of content of the vector
         * @param src The vector source to convert
         * @return list<T> A list of the same type content as the source vector
         */
        template <typename T>
        static list<T> VectToList(vector<T> src)
        {
            std::list<T> dest;
            dest.insert(dest.begin(), src.begin(), src.end());

            return dest;
        }

        /**
         * @brief Get a copy of the vector with unique value (remove duplicate) (copy will be ordered)
         * 
         * @tparam T The type of content of the vector
         * @param src The vector source to remove duplicate
         * @return vector<T> A sorted copy of the source without duplicate value
         */
        template <typename T>
        static vector<T> distinctVector(vector<T> source)
        {
            //https://en.cppreference.com/w/cpp/algorithm/unique
            sort(source.begin(), source.end());
            source.erase(unique(source.begin(), source.end()), source.end());

            return source;
        }
    };
} // namespace He_Arc::RPG
