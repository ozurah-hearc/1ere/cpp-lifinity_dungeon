/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   25.05.2021
 *
 * @file        :   Enums.hpp
 * @brief       :   This file contains public enums used in the project
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <map>
using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief The type of groups of characters
     * 
     */
    enum class TeamGrp
    {
        Team1,
        Team2,
        None
    };

    /**
     * @brief Get the Enum TeamGrp name as string
     * 
     */
    static map<TeamGrp, string> TeamGrpToString = {
        {TeamGrp::Team1, "Team1"},
        {TeamGrp::Team2, "Team2"},
        {TeamGrp::None, "None"}};

}