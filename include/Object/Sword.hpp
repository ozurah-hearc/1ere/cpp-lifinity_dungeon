/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   2.0
 * @date        :   09.03.2021
 *
 * @file        :   Sword.hpp
 * @brief       :   Header file, for the class "Sword.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include ".\IObject.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the swords, contains the stats and capacity of a sword
     * 
     */
    class Sword : public IObject
    {
    private:
        /**
         * @brief The damage which the sword deals
         * 
         */
        int m_damage{0};

    public:
        /**
         * @brief Construct a new Sword object
         * 
         * @param name name of the sword (for identifying it)
         * @param damage The damage which the sword deals
         */
        Sword(string name, int damage);
        // //Remplaced by GetFeature of IObject
        // /**
        //  * @brief Get the Damage of the sword
        //  *
        //  * @return int Damage of the sword
        //  */
        // int GetDamage() const;
        /**
         * @brief Get the feature (damage) of the sword
         * 
         * @return int Damage of the sword
         */
        int GetFeature() const override;
        /**
         * @brief Get the name of the sword
         * 
         * @return string The name of the sword
         */
        string GetName() const override;

        Sword &operator=(const Sword &sword);

    };
} // namespace He_Arc::RPG
