/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Shield.hpp
 * @brief       :   Header file, for the class "Shield.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include ".\IObject.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the shields, contains the stats and capacity of a shield
     *          A shield reduce damage taken by the character who held it
     * 
     */
    class Shield : public IObject
    {
    private:
        /**
         * @brief The solidity of the shield (damage reduced of the character)
         * 
         */
        int m_solidity{0};

    public:
        /**
         * @brief Construct a new Shield object
         * 
         * @param name name of the shield (for identifying it)
         * @param solidity The solidity of the shield (damage reduced of the character)
         */
        Shield(string name, int solidity);
        /**
         * @brief Get the feature (solidity) of the sword
         * 
         * @return int solidity of the shield
         */
        int GetFeature() const override;
        /**
         * @brief Get the name of the shield
         * 
         * @return string the name of the shield
         */
        string GetName() const override;

        Shield &operator=(const Shield &shield);
    };

} // namespace He_Arc::RPG
