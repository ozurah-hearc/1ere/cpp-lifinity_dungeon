/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Potion.hpp
 * @brief       :   Header file, for the class "Potion.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include ".\IObject.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the potions, contains the stats and capacity of a potion
     * 
     */
    class Potion : public IObject
    {
    private:
        /**
         * @brief The damage which the potion deals
         * 
         */
        int m_power{0};

    public:
        /**
         * @brief Construct a new Sword object
         * 
         * @param name name of the potion (for identifying it)
         * @param damage The damage which the potion deals
         */
        Potion(string name, int power);
        /**
         * @brief Get the feature (power) of the potion
         * 
         * @return int power of the potion
         */
        int GetFeature() const override;
        /**
         * @brief Get the name of the potion
         * 
         * @return string the name of the potion
         */
        string GetName() const override;
    };

} // namespace He_Arc::RPG
