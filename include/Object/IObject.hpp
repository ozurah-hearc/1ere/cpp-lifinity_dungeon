/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   IObject.hpp
 * @brief       :   Interface class, for the different objects who can be posseded by an characteur
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Interface class, for the different objects who can be posseded by an characteur
     * 
     */
    class IObject
    {
    protected:
        string m_name{"Unnamed object"};

    public:
        //virtual IObject(string name = "Unnamed object");
        virtual ~IObject() = default;
        /**
         * @brief Get the name of the object
         * 
         * @return string the name of the object
         */
        virtual string GetName() const = 0;
        /**
         * @brief Get the feature (the characteristic) of the object
         * 
         * @return int The characteristic of the object
         */
        virtual int GetFeature() const = 0;
    };

} // namespace He_Arc::RPG
