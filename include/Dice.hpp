/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   27.04.2021
 *
 * @file        :   Dice.hpp
 * @brief       :   Header and template file.
 *                  Class for the dice, gives you a random value depending the type of dice
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <stack>
#include <list>
#include <vector>
#include <random>
#include <chrono>           //For getting the current time (for generator)
#include <type_traits>      //For constructor specialisation
#include <initializer_list> //for using initializer_list

#include "./Utility.hpp"
#include "./Log/Log.hpp"
#include "./Log/LogCollection.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the dice, gives you a random value depending the type of dice
     * 
     * @tparam T Type of the faces of the dice
     */
    template <typename T>
    class Dice
    {
    private:
        /**
         * @brief Faces of the dice
         * @remark Use list because it is advised when we want to remove element anywhere in a collection
         */
        list<T> m_faces;
        /**
         * @brief Is the last roll get a minimal value 
         * @remark if the dice have face 1, 2, 3; will be True if roll get 1
         */
        bool m_wasMinimalRoll = false;
        /**
         * @brief Is the last roll get a maximal value 
         * @remark if the dice have face 1, 2, 3; will be True if roll get 3
         */
        bool m_wasMaximalRoll = false;
        /**
         * @brief the generator alogirthm used to get a random face
         * 
         */
        default_random_engine m_generator; // default_random_engine = simpliest gen // mt19937 = a better but cheaper gen
        /**
         * @brief Add to the log the creation of the dice
         * 
         */
        void LogDiceCreation();
        /**
         * @brief Set the generator algorithm for the dice
         * 
         */
        void BuildGen();

    public:
        /**
         * @brief Constructor for a int Dice only
         *        it set the faces from 1 to the number (included)
         * 
         * @param nbRegularFaces last number face (included), ie: if is 3, the dice will have the faces 1 2 3
         */
        Dice(int nbRegularFaces = 6) requires(std::is_same_v<T, int>)
        {
            BuildGen();

            m_faces.clear();
            for (int i = 1; i <= nbRegularFaces; ++i)
                m_faces.push_back(i);

            // Log the creation of the dice
            LogDiceCreation();
        };
        /**
         * @brief Construct a new Dice object
         * 
         * @param faces the faces of the dice
         */
        Dice(std::initializer_list<T> faces); //if we want to pass arguments like "{item1, item2}"
        /**
         * @brief Construct a new Dice object
         * 
         * @param faces the faces of the dice
         * @remark This is the main constructor
         */
        Dice(list<T> faces);
        /**
         * @brief Construct a new Dice object
         * 
         * @param faces the faces of the dice
         */
        Dice(vector<T> faces);
        /**
         * @brief Perform a roll of the dice and get a random value
         * 
         * @return T The value get after the roll
         */
        T Roll();
        /**
         * @brief Remove a face of the dice
         * 
         * @param face The face to remove
         */
        void RemoveFace(T face);

        /**
         * @brief Get the information if last roll was a minimal
         * 
         * @return true The last roll was a minimal value
         * @return false The last roll wasn't minimal value
         * @remark if the dice have face 1, 2, 3; will be True if roll get 1
         */
        bool GetWasMinimalRoll();
        /**
         * @brief Get the information if last roll was a maximal
         * 
         * @return true The last roll was a maximal value
         * @return false The last roll wasn't maximal value
         * @remark if the dice have face 1, 2, 3; will be True if roll get 3
         */
        bool GetWasMaximalRoll();
    };

    /**
     * @brief Construct a new Dice< T>:: Dice object
     * 
     * @tparam T Type of the faces of the dice
     * @param faces the faces of the dice
     */
    template <typename T>
    Dice<T>::Dice(std::initializer_list<T> faces) : Dice<T>(static_cast<list<T>>(faces)) {} //Call the main constructor

    /**
     * @brief Construct a new Dice< T>:: Dice object
     * 
     * @tparam T Type of the faces of the dice
     * @param faces the faces of the dice
     */
    template <typename T>
    Dice<T>::Dice(std::list<T> faces)
    {
        BuildGen();

        m_faces = faces;

        // Log the creation of the dice
        LogDiceCreation();
    }

    /**
     * @brief Construct a new Dice< T>:: Dice object
     * 
     * @tparam T Type of the faces of the dice
     * @param faces the faces of the dice
     */
    template <typename T>
    Dice<T>::Dice(std::vector<T> faces) : Dice(Utility::VectToList(faces))
    {
    }

    /**
     * @brief Add to the log the creation of the dice
     * @tparam T Type of the faces of the dice
     * 
     */
    template <typename T>
    void Dice<T>::LogDiceCreation()
    {
        std::stringstream stream;
        stream << "The dice " << this << " was created. It had " << m_faces.size() << " faces (address of faces : ";
        for (auto face : m_faces)
            stream << face << " ";
        stream << ")";
        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Game, Log::LogType::Dice,
                stream.str()}));
    }

    /**
     * @brief Get the information if last roll was a minimal
     * @tparam T Type of the faces of the dice
     * 
     * @return true The last roll was a minimal value
     * @return false The last roll wasn't minimal value
     * @remark if the dice have face 1, 2, 3; will be True if roll get 1
     */
    template <typename T>
    bool Dice<T>::GetWasMinimalRoll() { return m_wasMinimalRoll; }
    /**
     * @brief Get the information if last roll was a maximal
     * @tparam T Type of the faces of the dice
     * 
     * @return true The last roll was a maximal value
     * @return false The last roll wasn't maximal value
     * @remark if the dice have face 1, 2, 3; will be True if roll get 3
     */
    template <typename T>
    bool Dice<T>::GetWasMaximalRoll() { return m_wasMaximalRoll; }

    /**
     * @brief Perform a roll of the dice and get a random value
     * 
     * @tparam T Type of the faces of the dice
     * @return T The value get after the roll
     */
    template <typename T>
    T Dice<T>::Roll()
    {
        uniform_int_distribution<int> distribution(0, m_faces.size() - 1);
        auto index = distribution(m_generator); // Get a random value

        //minimal / maximal value
        m_wasMinimalRoll = (index == 0);
        m_wasMaximalRoll = (index == static_cast<int>(m_faces.size()) - 1);

        //Element get
        auto iterator = std::next(m_faces.begin(), index);

        //Log the roll
        std::stringstream stream;
        stream << "The dice " << this << " have roll the face number " << index << ". (It had " << m_faces.size() - 1 << " faces (0-n))";

        LogCollection::GetInstance()->AddLog(
            make_unique<Log>(Log{
                Log::LogLevel::Game, Log::LogType::Dice,
                stream.str()}));

        return *iterator;
    }

    /**
     * @brief Remove the face of the dice
     * 
     * @tparam T Type of the faces of the dice
     * @param face The face to remove
     */
    template <typename T>
    void Dice<T>::RemoveFace(T face)
    {
        //There is no problem if trying to remove a non existing face, no exception will throw
        m_faces.remove(face);
    }

    /**
     * @brief Set the generator algorithm for the dice
     * 
     * @tparam T Type of the faces of the dice
     */
    template <typename T>
    void Dice<T>::BuildGen()
    {
        static int modifier = 0; //Different random for each instance (without, if 2 dice instancied in a row, the generator will be the same)
        modifier++;

        //https://spc.unige.ch/en/teaching/courses/algorithmes-probabilistes/random-numbers-week-1/
        //m_generator(std::chrono::system_clock::now().time_since_epoch().count()); // Not working

        /*
        * the random will give new values after any execution of the code
        * The sequence will be unique for each instance
        */
        m_generator = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count() * modifier); //if use + instead of *, modifier had sometimes same sequences as without modifier
        /*
        * the random will give same values after any execution of the code
        * The sequence will be unique for each instance
        */
        //m_generator = std::default_random_engine(modifier);
        /*
        * the random will give same values after any execution of the code
        * The sequence will be the same, for instance declared in a row
        */
        // m_generator = std::default_random_engine();
    }
} // namespace He_Arc::RPG

/* OLD CODE
    // Old method constr specialisation
    
    // template <typename T>
    // Dice<T>::Dice(int nbRegularFaces)
    // {
    //     BuildGen();

    //     m_faces.clear();
    //     for (int i = 1; i <= nbRegularFaces; ++i)
    //         m_faces.push_back(i);
    // }

*/