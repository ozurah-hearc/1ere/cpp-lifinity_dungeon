/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   15.06.2021
 *
 * @file        :   Game.hpp
 * @brief       :   Header file, for the class "Game.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <tuple>

#include "./Character/CharacterClass.hpp"
#include "./Room/Room.hpp"
#include "./Room/Lane.hpp"
#include "./Log/Log.hpp"
#include "./Spell/ISpell.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class is the game logic implementation,
     *        It regroups each classes and interconnect them to perform a complet game
     * 
     */
    class Game
    {
    private:
        /**
         * @brief Number of ally (characters) of the player (without count invocations)
         * 
         */
        int m_nbAlly{4};
        /**
         * @brief Current level of the game, it determine the level of the ennemy team
         * @remark PerformStartRoom increment it when enter into a new room
         */
        int m_gameLevel{0};

        /**
         * @brief The fight room informations
         * 
         */
        unique_ptr<Room> m_fightingRoom;
        /**
         * @brief Characters of the player
         * 
         */
        vector<shared_ptr<CharacterClass>> m_playerTeam;
        /**
         * @brief Characters of the enemy team (who the player will defeat (or not !))
         * 
         */
        vector<shared_ptr<CharacterClass>> m_ennemyTeam;

        /**
         * @brief Contains the order of the fight, first element is the current playing character
         *  After each character turn, the order rotate
         * 
         */
        vector<shared_ptr<CharacterClass>> m_fightOrder;

        /**
         * @brief Create a the ennemy team composition, based on the level of the game
         *        When calling, the current ennemy team is cleaned
         */
        void CreateEnnemyTeamComposition();
        /**
         * @brief Set the turn order for the fight
         * @remark the order will be setted randomly
         */
        void SetTurnOrder();
        /**
         * @brief Perform a rotation in the m_fightOrder to having the next "character turn"
         * The "first character" in the collection will be go to the last
         * 
         */
        void GetNextPlayer();

        /**
         * @brief Perform a character turn for the player (display menu, user select, do action according the menu choose)
         * 
         */
        void DoPlayerCharacterTurn();

        /**
         * @brief Perform a character turn for the ennemy (auto play)
         * Check range of the spells/object of the character; 
         *      (actually 1 spell and object have range of 1)
         *      if can hit someone (of the player team), with a spell/object :
         *          Select a possible action (spell-skill / objet)
         *          Select a random character
         *      if can't hit; move 1 to the left
         *      
         */
        void DoEnnemyCharacterTurn();

        // ENNEMY ACTION
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Get the maximal range of their spell where he can hit someone
         * 
         * @return int The maximal range of their spell
         */
        int DoEnnemyTurnGetMaxRange();
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" : 
         *          Get the possible spells used within the range "1 to X"
         * @param range maximal range of their spell
         * @return tuple<bool, vector<shared_ptr<ISpell>>> bool : is the "object" is counted as possible action ?
         *                                                 vector<shared_ptr<ISpell>> : Collection of all possible spell for this range (do not use the value of this if the "bool" = true)
         */
        tuple<bool, vector<shared_ptr<ISpell>>> DoEnnemyTurnGetPossibleSpell(int range);
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Get a random spell
         * 
         * @param includeObject is the "object" is counted as possible action ?
         * @param possibleSpells Collection of all possible spell
         * @return tuple<bool, shared_ptr<ISpell>> bool : is the "object" is the selected "spell" ?
         *                                                 shared_ptr<ISpell> : Spell get (do not use the value of this if the "bool" = true)
         */
        tuple<bool, shared_ptr<ISpell>> DoEnnemyTurnGetRandomSpell(bool includeObject, vector<shared_ptr<ISpell>> possibleSpells);
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Get the targetable character within the range "1 to X"
         * @param range maximal range of the target
         * @return vector<shared_ptr<CharacterClass>> Collection of all possible target for this range
         */
        vector<shared_ptr<CharacterClass>> DoEnnemyTurnGetPossibleTarget(int range);
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Get a random target
         * @param possibleTargets Collection of all possible target
         * @return shared_ptr<CharacterClass> the target
         */
        shared_ptr<CharacterClass> DoEnnemyTurnGetRandomTarget(vector<shared_ptr<CharacterClass>> possibleTargets);
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Use the spell on the target
         * 
         * @param useObject is the "object" is the selected "spell" ?
         * @param spell the spell to use
         * @param target the target of the spell
         */
        void DoEnnemyTurnUseSpell(bool useObject, shared_ptr<ISpell> spell, shared_ptr<CharacterClass> target);
        /**
         * @brief Action done by the ennemy "IA" for "ennemy turn" :
         *          Move the character in one lane to the left
         */
        void DoEnnemyTurnMoveLeft();

        // MENU ACTION
        /**
         * @brief Action done when option choosen is "Pass"
         * 
         */
        void DoActionMenuPass();
        /**
         * @brief Action done when option choosen is "Move"
         * @return Is the action cancelled
         */
        bool DoActionMenuMove();
        /**
         * @brief Action done when option choosen is "Fight"
         * @return Is the action cancelled
         */
        bool DoActionMenuFight();
        /**
         * @brief Action done when option choosen is "Fight" (display menu for selecting skill/spell)
         * @return tuple<bool, shared_ptr<ISpell>> bool = Is the action cancelled ?
         *                                                 2nd bool : True = "use object" / False = use spell
         *                                                 ISpell the spell used (nullptr if no spell used)
         */
        tuple<bool, bool, shared_ptr<ISpell>> DoActionMenuFightSelectSpell();

        /**
         * @brief Action done when option choosen is "Fight" (selecting target)
         * @remark Must be used just after "DoActionMenuFightSelectSpell", for knowing the range of the spell
         * 
         * @param range The range of the target to get when building menu
         * 
         * @return tuple<bool, shared_ptr<CharacterClass>> bool = Is the action cancelled ?
         *                                                  CharacterClass = target
         */
        tuple<bool, shared_ptr<CharacterClass>> DoActionMenuFightSelectTarget(int range);

        /**
         * @brief Move all "dead character" (hp < 0) to the dead lanes of their team
         * @return bool true if the current character die (to avoid if the current character is die, to pass the turn of the next character)
         */
        bool MoveDeadCharacters();

    public:
        /**
         * @brief Construct a new Game object
         * 
         * @param nbAlly The number of ally available when creating the team
         */
        Game(int nbAlly = 4);
        Game(const Game &game);
        virtual ~Game() = default;
        /**
         * @brief Get the level of the game
         * 
         * @return int level of the game
         */
        int GetGameLevel();
        /**
         * @brief The assignation (in depth) of a game (copy)
         * 
         * @param game 
         * @return Game& 
         */
        Game &operator=(const Game &game);

        /**
         * @brief Create a the team of the player (add new character to the team)
         *        When calling, the current team is cleaned
         */
        void CreateTeamComposition();

        /**
         * @brief Go to a fight room, this function will set the ennemy team, and prepare the room for fighting
         * 
         */
        void EnterFightingRoom();
        /**
         * @brief Display the GUI for the fight
         * 
         */
        void DisplayFight() const;
        /**
         * @brief Perform a complet game turn
         * (player turn and monster turn)
         * 
         */
        void PerformGameTurn();
        /**
         * @brief Is a team is dead (each character dead)
         * 
         * @return TeamGrp The dead team indicator
         */
        TeamGrp ThereIsDeadTeam();
        /**
         * @brief Is all character in the team are dead (hp <= 0)
         * 
         * @param team team to analyse
         * @return true All characters of the team are dead
         * @return false Team have at least 1 character alive
         */
        bool IsDeadTeam(vector<shared_ptr<CharacterClass>> team);

        /**
         * @brief Increase the level of the game by 1
         * 
         */
        void IncreaseGameLevel();

        /**
         * @brief Do the action to enter into a room, do the fight, and finally finish the room
         * 
         */
        void PerformStartRoom();

        /**
         * @brief Heal the whole player's team
         *      (No check if character deads; they will be healed too; --> Miracle, he resuscitated)
         * @remark (resusciated character in fight is not handled; he will only be able to participate in the fight of the next room !)
         * 
         * @param amount How many health are given to the team of the player
         */
        void HealPlayer(int amount);
    };
} // namespace He_Arc::RPG
