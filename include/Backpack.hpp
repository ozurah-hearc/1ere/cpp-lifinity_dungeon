/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   5.0
 * @date        :   19.04.2021
 *
 * @file        :   Backpack.hpp
 * @brief       :   Header file, for the class "Backpack.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <stack>
#include ".\Object\IObject.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Backpack, It's an portable inventory, who can be held by a character during is adventure
     * 
     */
    class Backpack
    {
    private:
        /**
         * @brief Objects in the backpack
         * 
         */
        stack<shared_ptr<IObject>> m_stack;

    public:
        Backpack() = default;
        virtual ~Backpack() = default;
        /**
         * @brief Add an object to the backpack
         * 
         * @param pObject Object to add in the backpack
         */
        void Pack(shared_ptr<IObject> pObject);
        /**
         * @brief Remove the topside object of the backpack
         * 
         * @return shared_ptr<IObject> Object removed from the backpack
         *          If no object has been unpacked, return nullptr
         */
        shared_ptr<IObject> UnPack();
        /**
         * @brief Is the backpack contains object ?
         * 
         * @return true The backpack still has object(s)
         * @return false The backpack is empty
         */
        bool IsNotEmpty() const;
    };

} // namespace He_Arc::RPG
