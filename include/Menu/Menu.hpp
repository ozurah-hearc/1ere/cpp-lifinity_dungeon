/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   Menu.hpp
 * @brief       :   Header file, for the class "Menu.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include "./MenuItem.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class is menu interface,
     *        It regroups de functionality to display the menu, and choose an action
     * 
     */
    class Menu
    {
    private:
        /**
         * @brief Maximum of menu item displayed by the menu
         * @remark 26 for the alphabet A-Z
         */
        const int QTY_MENU_LIMIT{26};
        /**
         * @brief Title of the menu
         * 
         */
        string m_title{"no title menu"};
        /**
         * @brief Collection of menu items (action who can be selected in this menu)
         * 
         */
        vector<shared_ptr<MenuItem>> m_menus;
        /**
         * @brief Size (in character) of the menu
         * @remark the size isn't automatic according to the terminal size, but would be great to do this
         *          It's possible, but we will lose the cross plateform compatibility
         *          This is why use variable instead dynamic size
         */
        int m_menuSize{100};
        /**
         * @brief Character used to draw the border of the menu
         * 
         */
        string m_border{"-"};

        /**
         * @brief Write in the terminal all of the menus items
         * 
         * @param length Size (in character) of the menu
         * @param border Character(s) used to draw the border of the menu
         */
        void WriteMenusItem(int length, string border) const;

        /**
         * @brief Write in the terminal the header of the menu
         * 
         * @param length Size (in character) of the menu
         * @param border Character used to draw the border of the menu
         * @param text Text of the header
         */
        void WriteHeader(int length, string border, const string text) const;

    public:
        /**
         * @brief Construct a new Menu object
         * 
         * @param title title of the menu
         * @param menus collection of menu items who can be selected in this menu
         * @param size Size (in character) of the menu when displayed
         * @param border character(s) use to do the border (using string to allow to have special UTF8 characters)
         */
        Menu(string title, vector<shared_ptr<MenuItem>> menus, int size = 100, string border = "-");
        virtual ~Menu() = default;
        /**
         * @brief Display the home page of the program
         *          Close it by pressing "Enter"
         */
        static void DisplayHomePage();
        /**
         * @brief Display the "You Die" page of the program
         *          Close it by pressing "Enter"
         */
        static void DisplayYouDiePage();
        /**
         * @brief Display the menu with header and each possible options (menu items)
         *          (didn't display the input query)
         */
        void DisplayMenu() const;
        /**
         * @brief Input query to select a menu
         * 
         * @return char Character entered by the user
         */
        char AskMenuSelect() const;
        /**
         * @brief Get the type of the menu who the user wants to select
         * 
         * @param key The menu to select
         * @return MenuItem::MenuType The type of the selected menu
         */
        shared_ptr<MenuItem> SelectMenu(char key) const;
    };
} // namespace He_Arc::RPG
