/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   MenuItem.hpp
 * @brief       :   Header file, for the class "MenuItem.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This class is an item of the menu,
     *        By items, that means an option who will be displayed in the menu
     * 
     */
    class MenuItem
    {
    public:
        /**
         * @brief Type of actions who can done by the menu
         * 
         */
        enum MenuType
        {
            // Fight action
            Pass,
            Move,
            Fight,
            //For the fight
            MoveLeft,
            MoveRight,
            MoveTo,
            FightObject,
            FightSpell,
            //Other
            Interact,
            Percreption,
            Cancel,
            CharacterSelection,
            Default,
        };

    private:
        /**
         * @brief Associated key to the menu (to select it)
         * 
         */
        char m_key{'-'};
        /**
         * @brief Text displayed to indicate the menu action
         * 
         */
        string m_text{"unnamed menu"};
        /**
         * @brief Type of the menu
         * 
         */
        MenuType m_type{Default};
        /**
         * @brief pointer of the item linked to the menu (like a character, a object, etc)
         * @remark This is a void pointer, to allow to get any type of element
         */
        shared_ptr<void> m_linkedElement{nullptr};

    public:
        /**
         * @brief Construct a new Menu Item object
         * 
         * @param text Text displayed to indicate the menu action
         * @param type Type of menu action
         * @param key Key to select the menu; remark : key will be setted in Menu.cpp, but for custom menus, we can set key here
         */
        MenuItem(string text, MenuType type, shared_ptr<void> linkedElement = nullptr, char key = '-');
        virtual ~MenuItem() = default;
        /**
         * @brief Get the text who will be displayed to indicate the menu action 
         * 
         * @return string 
         */
        string GetTextMenuItem() const;
        /**
         * @brief Is the key is the one associated to the menu ?
         * 
         * @param key key to check if linked to this menu
         * @return true The menu is selected by this key
         * @return false The menu isn't selected by this key
         */
        bool IsThisMenuSelected(char key) const;
        /**
         * @brief Set the key to select this menu
         * 
         * @param key New key for this menu
         */
        void SetKey(char key);
        /**
         * @brief Get the Menu Type object
         * 
         * @return MenuType The type of action did by this menu
         */
        MenuType GetMenuType() const;
        /**
         * @brief Get the linked element to this menu
         * @code For using the correct element, use the code just bellow (with correct types and adaptation) :
         *       auto element = static_pointer_cast<Warrior>(GetLinkedElement()); //Now, we have the smart pointer of the type of the element
         *       after; you can use "element" as normal as a shared_ptr : element->Action()
         */
        shared_ptr<void> GetLinkedElement() const;
    };
} // namespace He_Arc::RPG
