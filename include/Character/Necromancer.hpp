/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   1.0
 * @date        :   28.03.2021
 *
 * @file        :   Necromancer.hpp
 * @brief       :   Header file, for the class "Necromancer.cpp", which one is a specialisation of Wizard
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include "./Wizard.hpp"
#include "../Object/IObject.hpp"
#include "../Object/Potion.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Necromancer, a class of character (Wizard specialisation)
     *        The necromancer is a wizard who deals with the deads. They can use their power and reanime the deads
     * 
     */
    class Necromancer : public Wizard
    {
    protected:
        /**
         * @brief The cost of mana of the "rise undeads spell" (rise undeads method)
         * 
         */
        const int COST_MANA_RISE_UNDEADS{2};

    public:
        /**
         * @brief Construct a new Necromancer object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         * @param mana The mana of the character, which allows him to use spells by consuming it
         * @param object The object held by the the character 
         */
        Necromancer(int level = 1, int strength = 3, int agility = 10, int intelligence = 16, int perception = 12, double hp = 8, string name = "no_name", int mana = 12, shared_ptr<IObject> object = make_shared<Potion>(Potion{"Normal Potion", 7}));
        virtual ~Necromancer() = default;
        /**
         * @brief Get the name of the class
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const override;
        /**
         * @brief Use the spell of the Necromancer : Some undeads entity is invoked
         * 
         */
        void RiseUndeads(); //For now, the spell isn't supposed to be used, the skill system will got a rework soon
    };
} // namespace He_Arc::RPG
