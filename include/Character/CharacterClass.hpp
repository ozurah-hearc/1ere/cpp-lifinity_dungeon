/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   1.0
 * @date        :   09.03.2021
 *
 * @file        :   CharacterClass.hpp
 * @brief       :   Header file, for the class "CharacterClass.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include "..\Object\IObject.hpp"
#include "..\Spell\ISpell.hpp"
#include "..\Backpack.hpp"
#include "..\Dice.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the CharacterClass. A character class is the "type" (the class) of a character, like a warrior or a wizard.
     *        This class contains the base attribute and commons methods of each type of character
     * 
     */
    class CharacterClass
    {
    protected:
        /**
         * @brief The dice used for rolls when a stats is used (like strengh, agility, ...)
         * 
         */
        Dice<int> m_statDice = Dice<int>(20);
        /**
         * @brief The level of the character
         * 
         */
        int m_level{1};
        /**
         * @brief The strength of the character.
         *        This characteristic is the physical power
         */
        int m_strength{0};
        /**
         * @brief The agility of the character.
         *        This characteristic is the dexterity, reflexes
         */
        int m_agility{0};
        /**
         * @brief The intelligence of the character.
         *        This characteristic is the mental power
         */
        int m_intelligence{0};
        /**
         * @brief The perception of the character.
         *        This characteristic is ability to examine a room, traps, see the vital point
         */
        int m_perception{0};
        /**
         * @brief The Health point (HP) of the character.
         *        This characteristic is the current health point
         */
        int m_hp{0};
        /**
         * @brief The name of the character.
         */
        string m_name{"no_name"};
        /**
         * @brief The object held by the the character.
         */
        shared_ptr<IObject> m_pObject{nullptr};
        /**
         * @brief The backpack (inventory) of the character.
         */
        shared_ptr<Backpack> m_backpack;
        /**
         * @brief Spells of the character
         * @remark normally, that would be a "vector<shared_ptr<ISpell>" but for now, the classes have only one spell
         *          Multiple spell will comming for a future version
         */
        shared_ptr<ISpell> m_spells;

    public:
        CharacterClass();
        CharacterClass(const CharacterClass &character);
        /**
         * @brief Construct a new Character Class object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         * @param object The object held by the the character
         */
        CharacterClass(int level, int strength, int agility, int intelligence, int perception, int hp, string name, shared_ptr<IObject> object = nullptr);
        virtual ~CharacterClass();
        /**
         * @brief Create a clone of the character
         * 
         * @return CharacterClass* The clonned character
         * @remark Source of the clone design for polymorphism :
         *      https://stackoverflow.com/questions/28273777/c-clone-function-in-abstract-class
         *      https://katyscode.wordpress.com/2013/08/22/c-polymorphic-cloning-and-the-crtp-curiously-recurring-template-pattern/
         *      https://stackoverflow.com/questions/6206977/copying-c-abstract-classes
         */
        virtual CharacterClass *Clone() const = 0;
        /**
         * @brief Get the name of the class
         *          For exemple; if called with this class, it would return "CharacterClass" or somthings like this
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const = 0;
        /**
         * @brief Show the statistics of the character
         * 
         */
        virtual void Show() const;
        /**
         * @brief Make an interaction between 2 characters
         * 
         * @param character The other character who interact
         */
        virtual void Interact(const CharacterClass &character) const = 0;
        /**
         * @brief Get the Agility of the character
         * 
         * @return int The agility of the character
         */
        int GetAgility() const;
        /**
         * @brief Get the Agility of the character
         * 
         * @return int The Strength of the character
         */
        int GetStrength() const;
        /**
         * @brief Get the Agility of the character
         * 
         * @return int The Intelligence of the character
         */
        int GetIntelligence() const;
        /**
         * @brief Get the HP of the character
         * 
         * @return int The HP of the character
         */
        int GetHp() const;
        /**
         * @brief Get the name of the character
         * 
         * @return string The name of the character
         */
        int GetPerception() const;
        /**
         * @brief Get the Perception of the character
         * 
         * @return string The name of the character
         */
        string GetName() const;
        /**
         * @brief Set the name of the character
         * 
         */
        void SetName(string name);
        /**
         * @brief Get the Level of the character
         * 
         * @return int Level of the character
         */
        int GetLevel() const;
        /**
         * @brief Increase the level of the character by one
         * 
         */
        void IncreaseLevel();
        /**
         * @brief Get the object held by the character
         * 
         * @return shared_ptr<IObject> The pointer of the object held by the character
         */
        shared_ptr<IObject> GetObject() const;

        /**
         * @brief Get the backpack held by the character
         * 
         * @return shared_ptr<Backpack> the pointer of the backpack held by the character
         */
        shared_ptr<Backpack> GetBackpack() const;
        /**
         * @brief Set the backpack held by the character
         * 
         * @param backpack Backpack to attribuate to the character
         */
        void SetBackpack(shared_ptr<Backpack> backpack);
        /**
         * @brief Get the each spell of the character
         * 
         * @return vector<shared_ptr<ISpell>> spells who can be rise by the character
         */
        shared_ptr<ISpell> GetSpells();

        /**
         * @brief Use the object held by the character
         * 
         * @param character The target on which to use the object (if nullptr, the target is this)
         */
        void UseObject(CharacterClass *character = nullptr);

        /**
         * @brief Use a skill/spell
         * 
         * @param target the target of the spell (if nullptr, the target is this)
         */
        virtual void UseSkill(CharacterClass *target = nullptr) = 0; // * allows to be nullable instead of &

        /**
         * @brief Deals damage to the character
         * 
         * @param damage the amount of damage the character takes
         */
        void SufferDamage(int damage);
        /**
         * @brief Heal the character
         * 
         * @param amount How many life points are given to the character
         */
        void Heal(int amount);

        /**
         * @brief The assignation (in depth) of a character (copy)
         * 
         * @param character 
         * @return CharacterClass& 
         */
        CharacterClass &operator=(const CharacterClass &character);

        /**
         * @brief Show the statistics of the character
         * 
         * @param s 
         * @param character The character to show
         * @return ostream& 
         */
        friend ostream &operator<<(ostream &s, const CharacterClass &character);
    };

    ostream &operator<<(ostream &s, const CharacterClass &character);

} // namespace He_Arc::RPG
