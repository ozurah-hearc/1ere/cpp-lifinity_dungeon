/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Warrior.hpp
 * @brief       :   Header file, for the class "Warrior.cpp", which one is a type of hero.
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include "./CharacterClass.hpp"
#include "../Object/IObject.hpp"
#include "../Object/Sword.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Warrior, a class of character (CharacterClass specialisation)
     *        The warriors use sword (and shields) and their strength to accomplish anything
     * 
     */
    class Warrior : public CharacterClass
    {
    protected:
        /**
         * @brief The damage deals by the tackle skill
         *  For the fun (Warriors thinks they have a secret power, let see that with their damage !!)
         */
        const int DAMAGE_SKILL_FULLPOWER{0};
        /**
         * @brief Range for the skill "Full Power"
         * 
         */
        const int RANGE_SKILL_FULLPOWER{1};

    public:
        /**
         * @brief Construct a new Warrior object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         * @param object The object held by the the character
         */
        Warrior(int level = 1, int strength = 15, int agility = 12, int intelligence = 5, int perception = 10, double hp = 12, string name = "no_name", shared_ptr<IObject> object = make_shared<Sword>(Sword{"Normal Sword", 7}));
        virtual ~Warrior() = default;
        /**
         * @brief Create a clone of the character
         * 
         * @return Warrior* The clonned character
         */
        virtual Warrior *Clone() const override;
        /**
         * @brief Get the name of the class
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const override;
        /**
         * @brief Make an interaction between 2 characters
         * 
         * @param character The other character who interact
         */
        void Interact(const CharacterClass &character) const override;
        /**
         * @brief Do a skill/spell to the entity
         * 
         * @param target the target of the skill
         */
        virtual void UseSkill(CharacterClass *target) override;
        Warrior &operator=(const Warrior &character);
    };
} // namespace He_Arc::RPG
