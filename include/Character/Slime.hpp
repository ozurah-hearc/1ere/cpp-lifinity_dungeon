/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Slime.hpp
 * @brief       :   Header file, for the class "Slime.cpp", which one is a type of Monster.
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include "./CharacterClass.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Slime, a class of monster (CharacterClass specialisation)
     *        A slime is a (small) monster who can be compared to a puddle of water. You can seen them more often like as a drop of water.
     * 
     */
    class Slime : public CharacterClass
    {
    protected:
        /**
         * @brief The damage deals by the tackle skill
         * 
         */
        const int DAMAGE_SKILL_TACKLE{1};
        /**
         * @brief Range for the skill "Tackle"
         * 
         */
        const int RANGE_SKILL_TACKLE{1};

    public:
        /**
         * @brief Construct a new Slime object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         */
        Slime(int level = 1, int strength = 4, int agility = 5, int intelligence = 0, int perception = 0, double hp = 3, string name = "no_name");
        virtual ~Slime() = default;
        /**
         * @brief Create a clone of the character
         * 
         * @return Slime* The clonned character
         */
        virtual Slime *Clone() const override;
        /**
         * @brief Get the name of the class
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const override;
        /**
         * @brief Show the statistics of the character
         * 
         */
        void Show() const override;
        /**
         * @brief Make an interaction between 2 characters
         * 
         * @param character The other character who interact
         */
        void Interact(const CharacterClass &character) const override;
        /**
         * @brief Do a skill/spell to the entity
         * 
         * @param target the target of the skill
         */
        virtual void UseSkill(CharacterClass *target) override;
    };
} // namespace He_Arc::RPG
