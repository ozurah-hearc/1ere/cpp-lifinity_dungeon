/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Skeleton.hpp
 * @brief       :   Header file, for the class "Skeleton.cpp", which one is a type of Monster.
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include "./CharacterClass.hpp"
#include "../Object/Sword.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Skeleton, a class of monster (CharacterClass specialisation)
     *        It's the dead characters of the lost times ?!
     * 
     */
    class Skeleton : public CharacterClass
    {
    protected:
        /**
         * @brief The damage deals by the Bones-Rang skill (pun with "Bones" and "Boomrang")
         * 
         */
        const int DAMAGE_SKILL_BONESRANG{3};
        /**
         * @brief Range for the skill "Bones-Rang"
         * 
         */
        const int RANGE_SKILL_BONESRANG{2};

    public:
        /**
         * @brief Construct a new Skeleton object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         */
        Skeleton(int level = 1, int strength = 6, int agility = 2, int intelligence = 1, int perception = 10, double hp = 15, string name = "no_name", shared_ptr<Sword> sword = make_shared<Sword>(Sword{"Rust Sword", 4}));
        virtual ~Skeleton() = default;
        /**
         * @brief Create a clone of the character
         * 
         * @return Skeleton* The clonned character
         */
        virtual Skeleton *Clone() const override;
        /**
         * @brief Get the name of the class
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const override;
        /**
         * @brief Show the statistics of the character
         * 
         */
        void Show() const override;
        /**
         * @brief Make an interaction between 2 characters
         * 
         * @param character The other character who interact
         */
        void Interact(const CharacterClass &character) const override;
        /**
         * @brief Do a skill/spell to the entity
         * 
         * @param target the target of the skill
         */
        virtual void UseSkill(CharacterClass *target) override;
        Skeleton &operator=(const Skeleton &character);
    };
} // namespace He_Arc::RPG
