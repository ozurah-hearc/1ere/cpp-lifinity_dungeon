/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   3.0
 * @date        :   28.03.2021
 *
 * @file        :   Wizard.hpp
 * @brief       :   Header file, for the class "Wizard.cpp", which one is a type of Hero.
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include "./CharacterClass.hpp"
#include "../Object/IObject.hpp"
#include "../Object/Potion.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the Wizard, a class of character (CharacterClass specialisation)
     *        The wizards have the ability to use some spells. They solve the problems with their intelligence
     */
    class Wizard : public CharacterClass //, enable_shared_from_this<Wizard> //This parts allow to use this as smart pointer (remplace "this" by "shared_from_this()")
    {
    protected:
        /**
         * @brief The damage deals by the fireball skill
         * 
         */
        const int DAMAGE_SKILL_FIREBALL{5};
        /**
         * @brief Range for the skill "FireBall"
         * 
         */
        const int RANGE_SKILL_FIREBALL{2};

        /**
         * @brief The cost of mana of the "cast spell" method
         * 
         */
        const int COST_MANA_CAST_SPELL{2};

        /**
         * @brief The amount of regened mana when the roll stats is a success
         * 
         */
        const int REGEN_MANA_AMOUT{1};

        /**
         * @brief The mana of the character, which allows him to use spells by consuming it
         * 
         */
        int m_mana{0};

    public:
        /**
         * @brief Construct a new Wizard object
         * 
         * @param level The level of the character
         * @param strength The strength of the character
         * @param agility The agility of the character
         * @param intelligence The intelligence of the character
         * @param perception The perception of the character
         * @param hp The Health point (HP) of the character
         * @param name The name of the character
         * @param mana The mana of the character, which allows him to use spells by consuming it
         * @param object The object held by the the character 
         */
        Wizard(int level = 1, int strength = 5, int agility = 10, int intelligence = 15, int perception = 10, double hp = 10, string name = "no_name", int mana = 10, shared_ptr<IObject> object = make_shared<Potion>(Potion{"Normal Potion", 4}));
        virtual ~Wizard() = default;
        /**
         * @brief Create a clone of the character
         * 
         * @return Wizard* The clonned character
         */
        virtual Wizard *Clone() const override;
        /**
         * @brief Get the name of the class
         * @remarks use it because "typeid(Class)" will show somethings like "Something95Something"
         * @return string An human identifying name for the class
         */
        virtual string GetClassTypeName() const override;
        /**
         * @brief Show the statistics of the character
         * 
         */
        void Show() const override;
        /**
         * @brief Make an interaction between 2 characters
         * 
         * @param character The other character who interact
         */
        void Interact(const CharacterClass &character) const override;
        /**
         * @brief Get the mana of the character
         * 
         * @return int The mana of the character
         */
        int GetMana() const;
        /**
         * @brief Do a skill/spell to the entity
         * 
         * @param target the target of the spell (if nullptr, the target is this)
         */
        virtual void UseSkill(CharacterClass *target = nullptr) override; // * allows to be nullable instead of &
        /**
         * @brief Get an amount of mana, according to the intelligence stat and 
         * if the roll
         * 
         */
        void RegenMana();
    };
} // namespace He_Arc::RPG
