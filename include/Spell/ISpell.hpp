/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   17.06.2021
 *
 * @file        :   ISpell.hpp
 * @brief       :   Interface class, for the different spells/skills who can be rise by characters
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
// #include "..\Character\CharacterClass.hpp" //Do not put here, else circular reference !  https://openclassrooms.com/forum/sujet/class-was-not-declared-in-this-scope-21430

using namespace std;

namespace He_Arc::RPG
{
    //use this instead for declar the class and avoid circular ref
    class CharacterClass;

    /**
     * @brief Interface class, for the different spell/skill who can be rise by characters
     * 
     */
    class ISpell
    {
    protected:
        /**
         * @brief Name of the spell / skill
         * 
         */
        string m_name{"Unnamed spell"};
        /**
         * @brief Range (number of lane from the user of spell) where the spell can be used
         * 
         */
        int m_range{0};

    public:
        virtual ~ISpell() = default;
        /**
         * @brief Get the name of the object
         * 
         * @return string the name of the object
         */
        virtual string GetName() const = 0;
        /**
         * @brief Range (number of lane from the user of spell) where the spell can be used
         * 
         * @return int range of the spell
         */
        virtual int GetRange() const = 0;
    };

} // namespace He_Arc::RPG
