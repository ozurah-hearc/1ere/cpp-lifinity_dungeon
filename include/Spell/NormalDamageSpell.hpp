/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   17.06.2021
 *
 * @file        :   NormalDamageSpell.hpp
 * @brief       :   Header file, for the class "NormalDamageSpell.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#pragma once

#include <iostream>
#include <memory>
#include ".\ISpell.hpp"
#include "..\Character\CharacterClass.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief Class for the normal damage spell/skill, that means it is rised by a character and  it only apply damage to a signle target
     * 
     */
    class NormalDamageSpell : public ISpell
    {
    private:
        /**
         * @brief The damage which the potion deals
         * 
         */
        int m_power{0};

    public:
        /**
         * @brief Construct a new FireBall Spell
         *  
         * @param name The name of the spell, for identifying it
         * @param range Range (number of lane from the user of spell) where the spell can be used
         * @param damage The damage which the spell
         */
        NormalDamageSpell(string name, int range, int power);
        /**
         * @brief Get the name of the spell
         * 
         * @return string the name of the spell
         */
        string GetName() const override;
        /**
         * @brief Range (number of lane from the user of spell) where the spell can be used
         * 
         * @return int range of the spell
         */
        virtual int GetRange() const override;
        /**
         * @brief Cast the spell to a target
         * 
         * @param user Character who rise the spell  
         * @param target Character who "get" the spell
         */
        void CastSpell(CharacterClass *user, CharacterClass *target);
    };

} // namespace He_Arc::RPG
