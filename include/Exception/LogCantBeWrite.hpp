/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   LogCantBeWrite.hpp
 * @brief       :   Exception file
 *                  This exception is intended to be rise when the the logs can be written in the specified file
 *                  The reason can be : The folder for the file didn't exist; the file is used by another program; etc.
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <stdexcept> // or #include <exception>
#include "..\Log\Log.hpp"

using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This exception is intended to be rise when the the logs can be written in the specified file
     *        The reason can be : The folder for the file didn't exist; the file is used by another program; etc.
     * 
     */
    class Ex_LogCantBeWrite : public exception
    {
    private:
        /**
         * @brief Information about the log who wasn't saved
         * 
         */
        Log *m_unwrittenLog;
        string m_msg;

    public:
        Ex_LogCantBeWrite(Log *unwrittenLog, string msg = "The log can't be write") noexcept : m_unwrittenLog(unwrittenLog), m_msg(msg) {}
        virtual ~Ex_LogCantBeWrite() { delete m_unwrittenLog; }
        const char *what() const noexcept override
        {
            string msg = m_msg + "\n" + m_unwrittenLog->ToString();
            return msg.c_str(); //c_str : string -> char*
        }
    };
} // namespace He_Arc::RPG
