/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   MenuItemNotFound.hpp
 * @brief       :   Exception file
 *                  This exception is intended to be rise when a nonexistant menu is selected
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <stdexcept> // or #include <exception>
using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This exception is intended to be rise when a nonexistant menu is selected
     * 
     */
    class Ex_MenuItemNotFound : public exception
    {
    private:
        string m_msg;

    public:
        Ex_MenuItemNotFound(char key, string msg = "There is no menu item assigned to the key ") noexcept : m_msg(msg + string(1, key)) {}
        virtual ~Ex_MenuItemNotFound() = default;
        const char *what() const noexcept override
        {
            return m_msg.c_str(); //c_str : string -> char*
        }
    };
} // namespace He_Arc::RPG