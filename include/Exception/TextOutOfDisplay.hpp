/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Delémont-Neuchâtel)
 *                  IT department
 * Module       :	C++ Programmation, 1242.3
 * @author      :   Jonas Allemann ISC1j
 * 
 *  Project     :	Fil Rouge
 *  Description :	The aim of this project is to put into practice the concepts seen during the course with a C++ project around a RPG game
 *
 * @version     :   6.0
 * @date        :   07.06.2021
 *
 * @file        :   TextOutOfDisplay.hpp
 * @brief       :   Exception file
 *                  This exception is intended to be rise when the text is longer than the destined area of where it is displayed
 *
 * @copyright Copyright (c) 2021
 */

#include <iostream>
#include <stdexcept> // or #include <exception>
using namespace std;

namespace He_Arc::RPG
{
    /**
     * @brief This exception is intended to be rise when the text is longer than the destined area of where it is displayed
     * 
     */
    class Ex_TextOutOfDisplay : public exception
    {
    private:
        string m_msg;

    public:
        Ex_TextOutOfDisplay(string msg = "The text is longer than the displayed area") noexcept : m_msg(msg) {}
        virtual ~Ex_TextOutOfDisplay() = default;
        const char *what() const noexcept override
        {
            return m_msg.c_str(); //c_str : string -> char*
        }
    };
} // namespace He_Arc::RPG
