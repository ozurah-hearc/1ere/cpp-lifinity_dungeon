var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw~",
  1: "bcdefgilmnprsuw",
  2: "bcdefgilmnprstuw",
  3: "acdeghilmnoprstuvw~",
  4: "cdfhlmnr",
  5: "lt",
  6: "o",
  7: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "related",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Friends",
  7: "Pages"
};

